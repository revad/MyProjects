package com.example.showjokelib;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ShowJokeActivity extends AppCompatActivity {

    public static final String KEY_JOKE = "extra.joke";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_joke);

        Intent intent = getIntent();
        TextView tv = findViewById(R.id.textView);
        tv.setText(intent.getStringExtra(KEY_JOKE));
    }
}
