package com.udacity.gradle.builditbigger;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.util.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.util.concurrent.CountDownLatch;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

//Used code from https://stackoverflow.com/a/44222992 with some modifications

@RunWith(AndroidJUnit4.class)
public class asyncTaskReturnsNonEmptyStringTest {

    private Context context;

    @Test
    public void testVerifyJoke() throws InterruptedException {
        assertTrue(true);
        final CountDownLatch latch = new CountDownLatch(1);
        context = InstrumentationRegistry.getContext();
        EndpointsAsyncTask testTask = new EndpointsAsyncTask()
        {

            @Override
            protected void onPreExecute() {}

            @Override
            protected void onPostExecute (String result) {
                assertNotNull(result);
                if (!TextUtils.isEmpty(result)){
                    assertTrue(result.length() > 0);
                    latch.countDown();
                }
            }
        };
        testTask.execute( new Pair<Context, String>(context, "Manfred") );
        latch.await();
    }
}
