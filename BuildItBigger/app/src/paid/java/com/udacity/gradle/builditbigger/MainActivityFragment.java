package com.udacity.gradle.builditbigger;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivityFragment extends Fragment {

    private MainActivityFragment.Callbacks mCallbacks;
    @BindView(R.id.show_joke_button) Button showjokeButton;
    @BindView(R.id.spinner_widget) ProgressBar spinner;

    public interface Callbacks {
        void onShowJokeButtonClick();
    }

    public MainActivityFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = null;
        if (context instanceof Activity) {
            activity = (Activity) context;
        }
        mCallbacks = (MainActivityFragment.Callbacks) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, root);

        spinner.setVisibility(View.GONE);

        showjokeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onShowJokeButtonClick();
            }
        });

        return root;
    }
}
