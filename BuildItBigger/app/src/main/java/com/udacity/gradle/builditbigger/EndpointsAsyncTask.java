package com.udacity.gradle.builditbigger;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.test.suitebuilder.annotation.Suppress;
import android.view.View;
import android.widget.ProgressBar;
import com.example.showjokelib.ShowJokeActivity;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.udacity.gradle.builditbigger.backend.myApi.MyApi;
import java.io.IOException;
import static android.support.v4.content.ContextCompat.startActivity;
import static com.example.showjokelib.ShowJokeActivity.KEY_JOKE;

public class EndpointsAsyncTask extends AsyncTask<Context, Void, String> {

    private MyApi myApiService = null;
    private Context context;
    private ProgressBar spinner;

    public EndpointsAsyncTask(MainActivity activity) {
        spinner = activity.findViewById(R.id.spinner_widget);
    }

    public EndpointsAsyncTask() {}

    @Override
    protected void onPreExecute() {
        spinner.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(Context... params) {
        if(myApiService == null) {  // Only do this once
            MyApi.Builder builder = new MyApi.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    // options for running against local devappserver
                    // - 10.0.2.2 is localhost's IP address in Android emulator
                    // - turn off compression when running against local devappserver
                    .setRootUrl("http://10.0.2.2:8080/_ah/api/")
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                        @Override
                        public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                            abstractGoogleClientRequest.setDisableGZipContent(true);
                        }
                    });
            // end options for devappserver

            myApiService = builder.build();
        }

        context = params[0];

        try {
            return myApiService.getJoke().execute().getData();
        } catch (IOException e) {
            return e.getMessage();
        }
    }

    @Suppress
    @Override
    protected void onPostExecute(String result) {
        spinner.setVisibility(View.INVISIBLE);
        Intent intent = new Intent(context, ShowJokeActivity.class);
        intent.putExtra(KEY_JOKE, result);
        startActivity(context,  intent, null);
    }
}
