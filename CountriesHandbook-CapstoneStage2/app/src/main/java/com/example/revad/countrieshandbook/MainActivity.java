package com.example.revad.countrieshandbook;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements CardsAdapter.ListItemClickListener {

    public static final String APP_MODE = "app_mode";
    public static final String SEARCH_MODE_BY_NAME = "search_by_name";
    public static final String SEARCH_MODE_BY_CAPITAL_CITY = "search_by_capital_city";
    public static final String SEARCH_MODE_BY_REGION = "search_by_region";
    public static final String SEARCH_MODE_ALL_COUNTRIES = "all_countries_list";
    public static final String SEARCH_MODE_SHOW_HISTORY = "show_history";
    public static final String SEARCH_MODE_BY_LANGUAGE = "search_by_language";
    public static final String COUNTRY_NAME_EXTRA = "country_name";
    public static final String COUNTRY_JSON_DATA_EXTRA = "country_json_data";
    private static final String ADS_KEY = "ca-app-pub-3940256099942544~3347511713";
    private CardsAdapter mAdapter;
    private RecyclerView mCardsList;
    private ArrayList<String> cards;
    final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_tool_bar);
        setSupportActionBar(toolbar);

        MobileAds.initialize(this, ADS_KEY);
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        cards = new ArrayList<String>( Arrays.asList(getResources().getStringArray(R.array.cards_names)) );

        mCardsList = findViewById(R.id.rv_countries_main);
        GridLayoutManager layoutManager = new GridLayoutManager(this, numberOfColumns());
        mCardsList.setLayoutManager(layoutManager);
        mCardsList.setHasFixedSize(true);

        mAdapter = new CardsAdapter(this, cards);
        mCardsList.setAdapter(mAdapter);

        if (getIntent().hasExtra(APP_MODE)) {
            getIntent().removeExtra(APP_MODE);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onListItemClick("", 5);
                }
            },100);
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private int numberOfColumns() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthDivider = 400;
        int width = displayMetrics.widthPixels;
        int nColumns = width / widthDivider;
        if (nColumns < 2 | nColumns == 2) return 2;
        return 3;
    }

    @Override
    public void onListItemClick(String cardData, int position) {
        if (position != 5 && !isNetworkAvailable()) {
            Toast.makeText(this, getString(R.string.no_internet_connection_limited_mode_message), Toast.LENGTH_SHORT).show();
            return;
        }
        Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(this).toBundle();
        switch (position) {
            case 0: {
                Intent searchByNameIntent = new Intent(MainActivity.this, SearchByNameActivity.class);
                searchByNameIntent.putExtra(SEARCH_MODE_BY_NAME, "");
                startActivity(searchByNameIntent, bundle);
                break;
            }
            case 1: {
                Intent searchByLanguageIntent = new Intent(MainActivity.this, SearchByLanguageActivity.class);
                startActivity(searchByLanguageIntent, bundle);
                break;
            }
            case 2: {
                Intent searchByCapitalCityIntent = new Intent(MainActivity.this, SearchByNameActivity.class);
                searchByCapitalCityIntent.putExtra(SEARCH_MODE_BY_CAPITAL_CITY, "");
                startActivity(searchByCapitalCityIntent, bundle);
                break;
            }
            case 3: {
                Intent searchByRegionIntent = new Intent(MainActivity.this, SearchByLanguageActivity.class);
                searchByRegionIntent.putExtra(SEARCH_MODE_BY_REGION, "");
                startActivity(searchByRegionIntent, bundle);
                break;
            }
            case 4: {
                Intent allCountriesListIntent = new Intent(MainActivity.this, SearchByNameActivity.class);
                allCountriesListIntent.putExtra(SEARCH_MODE_ALL_COUNTRIES, "");
                startActivity(allCountriesListIntent, bundle);
                break;
            }
            case 5: {
                Intent allCountriesListIntent = new Intent(MainActivity.this, SearchByNameActivity.class);
                allCountriesListIntent.putExtra(SEARCH_MODE_SHOW_HISTORY, "");
                startActivity(allCountriesListIntent, bundle);
                break;
            }
        }
    }
}