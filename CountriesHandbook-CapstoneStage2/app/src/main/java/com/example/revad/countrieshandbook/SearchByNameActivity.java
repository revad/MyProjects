package com.example.revad.countrieshandbook;

import android.app.ActivityOptions;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.LoaderManager;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.revad.countrieshandbook.db.CountryContract;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchByNameActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<CountryDataShort>>, SearchByNameAdapter.ListItemClickListener {

    @BindView(R.id.spinner_widget_search_by_name) ProgressBar spinner;
    @BindView(R.id.rv_countries_by_name) RecyclerView mCountriesList;
    @BindView(R.id.empty_state_view_search_by_name) TextView emptyStateView;
    @BindView(R.id.searchView_countries_by_name) SearchView searchView;
    private static final int LOADER_ID_GET_COUNTRIES_DATA_SHORT = 135246;
    private static final int LOADER_ID_GET_HISTORY = 135247;
    private static final String SAVED_FILTER_STRING = "filter_string";
    private static final String SAVED_RECYCLERVIEW_POSITION = "recyclerview_position";
    private static final String SAVED_LANGUAGE_LIST = "language_list";
    private static final String SAVED_SEARCH_MODE = "search_mode";
    private static final String SAVED_REGION = "region";
    private static final String SAVED_ADAPTER_DATA = "adapter_data";
    private SearchByNameAdapter mAdapter;
    private String[] languageList;
    private String searchMode;
    private String searchString;
    private int recyclerviewPosition = -1;
    private String mRegion;
    private GridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_name);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.search_by_name_tool_bar);
        setSupportActionBar(toolbar);

        layoutManager = new GridLayoutManager(this, numberOfColumns());
        mCountriesList.setLayoutManager(layoutManager);
        mCountriesList.setHasFixedSize(true);

        if (savedInstanceState != null ) {
            searchMode = savedInstanceState.getString(SAVED_SEARCH_MODE);
            recyclerviewPosition = savedInstanceState.getInt(SAVED_RECYCLERVIEW_POSITION);
        } else
        if (getIntent().hasExtra(MainActivity.SEARCH_MODE_BY_NAME)) {
            searchMode = MainActivity.SEARCH_MODE_BY_NAME;
        } else
        if (getIntent().hasExtra(MainActivity.SEARCH_MODE_BY_LANGUAGE)) {
            searchMode = MainActivity.SEARCH_MODE_BY_LANGUAGE;
        } else
        if (getIntent().hasExtra(MainActivity.SEARCH_MODE_BY_REGION)) {
            searchMode = MainActivity.SEARCH_MODE_BY_REGION;
        } else
        if (getIntent().hasExtra(MainActivity.SEARCH_MODE_BY_CAPITAL_CITY)) {
            searchMode = MainActivity.SEARCH_MODE_BY_CAPITAL_CITY;
        } else
        if (getIntent().hasExtra(MainActivity.SEARCH_MODE_ALL_COUNTRIES)) {
            searchMode = MainActivity.SEARCH_MODE_ALL_COUNTRIES;
        } else
        if (getIntent().hasExtra(MainActivity.SEARCH_MODE_SHOW_HISTORY)) {
            searchMode = MainActivity.SEARCH_MODE_SHOW_HISTORY;
        }

        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_LANGUAGE)) {
            if (savedInstanceState != null) languageList = savedInstanceState.getStringArray(SAVED_LANGUAGE_LIST);
                    else languageList = getIntent().getExtras().getStringArray(MainActivity.SEARCH_MODE_BY_LANGUAGE);
            searchView.setVisibility(View.GONE);
        }
        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_NAME)) {
            searchView.setVisibility(View.VISIBLE);
            setupSearchView();
            if (savedInstanceState != null) {
                searchString = savedInstanceState.getString(SAVED_FILTER_STRING);
                searchView.setQuery(searchString, true);
            }
        }
        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_CAPITAL_CITY)) {
            searchView.setVisibility(View.VISIBLE);
            setupSearchView();
            if (savedInstanceState != null) {
                searchString = savedInstanceState.getString(SAVED_FILTER_STRING);
                searchView.setQuery(searchString, true);
            }
        }
        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_REGION)) {
            if (savedInstanceState != null) mRegion = savedInstanceState.getString(SAVED_REGION);
                else mRegion = getIntent().getStringExtra(MainActivity.SEARCH_MODE_BY_REGION);
            searchView.setVisibility(View.GONE);
        }
        if (searchMode.equals(MainActivity.SEARCH_MODE_ALL_COUNTRIES)) {
            searchView.setVisibility(View.GONE);
        }
        if (searchMode.equals(MainActivity.SEARCH_MODE_SHOW_HISTORY)) {
            searchView.setVisibility(View.GONE);
            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                @Override
                   public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                       return false;
                   }
                @Override
                   public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    String[] args = {mAdapter.getCountryData(viewHolder.getAdapterPosition()).getName()};
                       getApplicationContext().getContentResolver().delete(
                               CountryContract.CountryEntry.CONTENT_URI,
                               CountryContract.CountryEntry.COLUMN_COUNTRY_FULL_NAME + "=?",
                               args);
                       mAdapter.removeItem(viewHolder.getAdapterPosition());
                       CountriesHandbookAppWidget.widgetRefreshBroadcast(getApplicationContext());
                   }
               };
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mCountriesList);
            Bundle args = new Bundle();
            getLoaderManager().initLoader(LOADER_ID_GET_HISTORY, args, this);
        }
        else
        if (isNetworkAvailable()) {
            Bundle args = new Bundle();
            if (searchMode.equals(MainActivity.SEARCH_MODE_BY_LANGUAGE))
                args.putStringArray(MainActivity.SEARCH_MODE_BY_LANGUAGE, languageList);
            if (searchMode.equals(MainActivity.SEARCH_MODE_BY_REGION))
                args.putString(MainActivity.SEARCH_MODE_BY_REGION, mRegion);
            getLoaderManager().initLoader(LOADER_ID_GET_COUNTRIES_DATA_SHORT, args, this);
        } else {
            spinner.setVisibility(View.GONE);
            emptyStateView.setText(R.string.no_network);
            emptyStateView.setVisibility(View.VISIBLE);
            Toast.makeText(this, getString(R.string.search_unavailable_message), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private void appWidgetUpdate() {
        Intent intent = new Intent(this, CountriesHandbookAppWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(
                new ComponentName(getApplication(), CountriesHandbookAppWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent);
    }

    private void setupSearchView() {
        searchView.setQueryHint(getString(R.string.search_here_hint));
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mAdapter != null) mAdapter.filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (mAdapter != null) {
                    if (!TextUtils.isEmpty(query)) mAdapter.filter(query);
                    else mAdapter.filter("Mock string to search");
                }
                return false;
            }
        });
    }

    private int numberOfColumns() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthDivider = 700;
        int width = displayMetrics.widthPixels;
        int nColumns = width / widthDivider;
        return nColumns;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putString(SAVED_FILTER_STRING, searchView.getQuery().toString());
        outState.putInt(SAVED_RECYCLERVIEW_POSITION, layoutManager.findFirstVisibleItemPosition());
        outState.putStringArray(SAVED_LANGUAGE_LIST, languageList);
        outState.putString(SAVED_SEARCH_MODE, searchMode);
        outState.putString(SAVED_REGION, mRegion);
        //outState.putParcelableArrayList(SAVED_ADAPTER_DATA, mAdapter.getCountriesData());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SAVED_FILTER_STRING, searchView.getQuery().toString());
        outState.putInt(SAVED_RECYCLERVIEW_POSITION, layoutManager.findFirstVisibleItemPosition());
        outState.putStringArray(SAVED_LANGUAGE_LIST, languageList);
        outState.putString(SAVED_SEARCH_MODE, searchMode);
        outState.putString(SAVED_REGION, mRegion);
        //outState.putParcelableArrayList(SAVED_ADAPTER_DATA, mAdapter.getCountriesData());
    }

    @NonNull
    @Override
    public Loader<ArrayList<CountryDataShort>> onCreateLoader(int i, @Nullable Bundle bundle) {
        spinner.setVisibility(View.VISIBLE);
        if (i == LOADER_ID_GET_HISTORY) {
            Uri countryQueryUri = CountryContract.CountryEntry.CONTENT_URI;
            return new DataLoader(this, countryQueryUri);
        }
        if (bundle.containsKey(MainActivity.SEARCH_MODE_BY_LANGUAGE) | bundle.containsKey(MainActivity.SEARCH_MODE_BY_REGION))
            return new DataLoader(this, bundle);
        return new DataLoader(this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<CountryDataShort>> loader, ArrayList<CountryDataShort> countryDataShort) {
        spinner.setVisibility(View.GONE);
        appWidgetUpdate();
        if (countryDataShort.size() == 0) {
            emptyStateView.setText(R.string.no_countries_found);
            emptyStateView.setVisibility(View.VISIBLE);
            if (mAdapter != null) mAdapter.clear();
            return;
        }
        mCountriesList.scrollToPosition(0);
        emptyStateView.setVisibility(View.INVISIBLE);
        if (mAdapter == null) {
            mAdapter = new SearchByNameAdapter(this, countryDataShort);
            mAdapter.setSearchMode(searchMode);
            if (searchMode.equals(MainActivity.SEARCH_MODE_BY_NAME) | searchMode.equals(MainActivity.SEARCH_MODE_BY_CAPITAL_CITY)) {
                if ( TextUtils.isEmpty(searchString) ) mAdapter.filter("Mock string to search");
                else mAdapter.filter(searchString);
            }
            mCountriesList.setAdapter(mAdapter);
            if (recyclerviewPosition != -1) {
                layoutManager.scrollToPosition(recyclerviewPosition);
            }
        }
        getLoaderManager().destroyLoader(LOADER_ID_GET_COUNTRIES_DATA_SHORT);
        getLoaderManager().destroyLoader(LOADER_ID_GET_HISTORY);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<CountryDataShort>> loader) { }

    @Override
    public void onListItemClick(View v, CountryDataShort cData, int position) {
        if (!isNetworkAvailable() && !searchMode.equals(MainActivity.SEARCH_MODE_SHOW_HISTORY)) {
            Toast.makeText(this, getString(R.string.search_unavailable_message), Toast.LENGTH_SHORT).show();
            return;
        }
        Intent showCountryInfoIntent;
        Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(this, v, v.getTransitionName()).toBundle();
        showCountryInfoIntent = new Intent(SearchByNameActivity.this, ShowCountryInfo.class);
        showCountryInfoIntent.putExtra(MainActivity.COUNTRY_NAME_EXTRA, mAdapter.getCountryData(position).getName());
        if (searchMode.equals(MainActivity.SEARCH_MODE_SHOW_HISTORY)) showCountryInfoIntent.putExtra(MainActivity.COUNTRY_JSON_DATA_EXTRA, mAdapter.getCountryData(position).getJSONData());
        startActivity(showCountryInfoIntent, bundle);
    }
}
