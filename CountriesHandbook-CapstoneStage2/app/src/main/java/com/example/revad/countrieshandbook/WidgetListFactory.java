package com.example.revad.countrieshandbook;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.example.revad.countrieshandbook.db.CountryContract;
import java.util.ArrayList;

public class WidgetListFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context mContext;
    private ArrayList<CountryDataShort> rCountries = new ArrayList<>();
    private String sortOrder = CountryContract.CountryEntry.COLUMN_COUNTRY_FULL_NAME + " ASC";

    Uri countryQueryUri = CountryContract.CountryEntry.CONTENT_URI;
    private  static final String[] MAIN_PROJECTION = {
            CountryContract.CountryEntry.COLUMN_COUNTRY_FULL_NAME,
            CountryContract.CountryEntry.COLUMN_COUNTRY_CAPITAL_CITY,
            CountryContract.CountryEntry.COLUMN_COUNTRY_FLAG,
            CountryContract.CountryEntry.COLUMN_COUNTRY_DATA,
    };

    public WidgetListFactory(Context context) {
        mContext = context;
    }

    @Override
    public void onCreate() {
        getData();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews wList = new RemoteViews(mContext.getPackageName(), R.layout.widget_list_item);
        wList.setTextViewText(R.id.widget_country_name_short, rCountries.get(position).getName());
        wList.setTextViewText(R.id.widget_capital_city_short, rCountries.get(position).getCapitalCity());
        return wList;
    }

    @Override
    public int getCount() {
        if (rCountries != null) { return rCountries.size(); }
        else return 1;
    }

    @Override
    public void onDataSetChanged() {
        getData();
    }

    @Override
    public int getViewTypeCount() {return 1;}

    @Override
    public long getItemId(int position) {return position;}

    @Override
    public void onDestroy() { }

    @Override
    public boolean hasStableIds() {return true;}

    @Override
    public RemoteViews getLoadingView() {return null;}

    protected void getData() {
        rCountries.clear();
        Cursor cursor = mContext.getContentResolver().query(countryQueryUri, MAIN_PROJECTION, null,
                null, sortOrder, null);
        while(cursor.moveToNext()) {
            rCountries.add( new CountryDataShort(
                    cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[0])),
                    cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[1])),
                    cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[2])),
                    null,
                    cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[3]))
            ) );
        }
    }
}
