package com.example.revad.countrieshandbook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;
import static android.support.constraint.Constraints.TAG;

public class MapFragment extends Fragment  implements OnMapReadyCallback {
    private GoogleMap mMap;
    private MapView mMapView;
    private View view;
    private Bundle shareData;

    public MapFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        shareData = this.getArguments();

        try {
            view = inflater.inflate(R.layout.fragment_map, container, false);
            MapsInitializer.initialize(this.getActivity());
            mMapView = view.findViewById(R.id.map);
            mMapView.onCreate(savedInstanceState);
            mMapView.getMapAsync(this);
        }
        catch (InflateException e){
            Log.e(TAG, "Inflate exception");
        }
        return view;
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        ArrayList<String> latLng = shareData.getStringArrayList(ShowCountryInfo.GEO_KEY);
        if (!latLng.isEmpty()) {
            String name = shareData.getString(ShowCountryInfo.NAME_KEY);
            LatLng capitalCity = new LatLng(Double.parseDouble(latLng.get(0)), Double.parseDouble(latLng.get(1)));
            mMap.addMarker(new MarkerOptions().position(capitalCity).title(name));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(capitalCity));
        }
    }
}
