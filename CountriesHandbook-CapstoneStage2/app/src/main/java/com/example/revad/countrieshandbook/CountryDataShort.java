package com.example.revad.countrieshandbook;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.SortedSet;

public class CountryDataShort implements Parcelable {

    private String mName;
    private String mCapitalCity;
    private String mFlag;
    private SortedSet<String> mLanguages;
    private String mJSONData;

    public CountryDataShort (String name, String capitalCity, String flag, SortedSet<String> languages, String JSONData) {
        mName = name;
        mCapitalCity = capitalCity;
        mFlag = flag;
        mLanguages = languages;
        mJSONData = JSONData;
    }

    protected CountryDataShort(Parcel in) {
        mName = in.readString();
        mCapitalCity = in.readString();
        mFlag = in.readString();
        mJSONData = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mCapitalCity);
        dest.writeString(mFlag);
        dest.writeString(mJSONData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CountryDataShort> CREATOR = new Creator<CountryDataShort>() {
        @Override
        public CountryDataShort createFromParcel(Parcel in) {
            return new CountryDataShort(in);
        }

        @Override
        public CountryDataShort[] newArray(int size) {
            return new CountryDataShort[size];
        }
    };

    public String getName () {return mName;}

    public String getCapitalCity () {return mCapitalCity;}

    public String getFlag() {return mFlag;}

    public SortedSet<String> getLanguages() {return mLanguages;}

    public String getJSONData() {return mJSONData;}

}
