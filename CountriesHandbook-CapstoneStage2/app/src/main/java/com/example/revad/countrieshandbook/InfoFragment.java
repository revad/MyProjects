package com.example.revad.countrieshandbook;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.bumptech.glide.RequestBuilder;
import java.util.ArrayList;

public class InfoFragment extends Fragment {

    private ImageView flagImageView;
    private Bundle shareData;
    private View view;

    public InfoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_info, container, false);
        shareData = this.getArguments();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        countryDisplayInfo(getString(R.string.name_label), shareData.getString(ShowCountryInfo.NAME_KEY));

        countryDisplayInfo(getString(R.string.native_name_label), shareData.getString(ShowCountryInfo.NATIVE_NAME_KEY));

        countryDisplayInfo(getString(R.string.capital_city_label), shareData.getString(ShowCountryInfo.CAPITAL_CITY_KEY));

        loadFlagImage(shareData.getString(ShowCountryInfo.FLAG_KEY));
        imageSetup();

        countryDisplayInfo(getString(R.string.population_label), shareData.getString(ShowCountryInfo.POPULATION_KEY));

        countryDisplayInfo(getString(R.string.languages_label), shareData.getStringArrayList(ShowCountryInfo.LANGUAGES_KEY));
        countryDisplayInfo(getString(R.string.geo_label), shareData.getStringArrayList(ShowCountryInfo.GEO_KEY));
        countryDisplayInfo(getString(R.string.time_zone_label), shareData.getStringArrayList(ShowCountryInfo.TIMEZONES_KEY));
        countryDisplayInfo(getString(R.string.currency_label), shareData.getStringArrayList(ShowCountryInfo.CURRENCIES_KEY));
    }

    private void countryDisplayInfo (String name, String data) {
        LayoutInflater inflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TableLayout table = view.findViewById(R.id.ingredients_list_layout);

        TableRow rowView = (TableRow)inflater1.inflate(R.layout.fragment_info_row, null);

        TextView iName = rowView.findViewById(R.id.name_view);
        iName.setText(name);

        TextView iAmount = rowView.findViewById(R.id.value_view);
        iAmount.setText(data);

        table.addView(rowView);
    }

    private void countryDisplayInfo (String name, ArrayList<String> data) {

        LayoutInflater inflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TableLayout table = getActivity().findViewById(R.id.ingredients_list_layout);

        TableRow rowView = (TableRow) inflater1.inflate(R.layout.fragment_info_row, null);

        final TextView iName = rowView.findViewById(R.id.name_view);
        final TextView iValue = rowView.findViewById(R.id.value_view);

        if (name.equals(getString(R.string.geo_label))) {
            iName.setTextColor(Color.BLUE);
            iValue.setTextColor(Color.BLUE);
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TabLayout tabs = (TabLayout)((ShowCountryInfo)getActivity()).findViewById(R.id.tab_layout);
                    tabs.getTabAt(1).select();
                }
            });
        }

        iName.setText(name);
        for (int i=0; i < data.size(); i++) {
            if (i > 0) iValue.append("\n");

            if ( name.equals(getString(R.string.geo_label)) ) {
                if (i % 2 == 0) iValue.append(data.get(i) + " Lat");
                else iValue.append(data.get(i) + " Lng");
            }
            else iValue.append(data.get(i));
        }

        table.addView(rowView);
    }

    private void loadFlagImage (String path) {
        RequestBuilder<PictureDrawable> requestBuilder;
        flagImageView = view.findViewById(R.id.flagImage1);
        requestBuilder = GlideApp.with(this)
                .as(PictureDrawable.class)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.error_placeholder).listener(new com.example.revad.countrieshandbook.SvgSoftwareLayerSetter());

        Uri uri = Uri.parse(path);

        requestBuilder.load(uri).into(flagImageView);
        flagImageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    private void imageSetup () {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        ViewGroup.LayoutParams params = flagImageView.getLayoutParams();
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            params.width = displayWidth - 128;
            params.height = (int) (params.width * 2 / 3);
            flagImageView.setLayoutParams(params);
        } else {
            params.width = (int) displayWidth / 2 - 300;
            params.height = (int) (params.width / 1.5);
            flagImageView.setLayoutParams(params);
        }
    }
}
