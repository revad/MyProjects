package com.example.revad.countrieshandbook;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

public class StringAdapter extends RecyclerView.Adapter<StringAdapter.LanguagesViewHolder> {

    private ArrayList<String> mLanguageData;
    final private StringAdapter.ListItemClickListener mOnClickListener;
    private Context context;
    boolean checked;
    private SortedSet<String> languagesSelected = new TreeSet<>();
    private String searchMode = "";

    public interface ListItemClickListener {
        void onListItemClick(String cardData, int position);
    }

    public StringAdapter(StringAdapter.ListItemClickListener listener, ArrayList<String> languages) {
        mOnClickListener = listener;
        mLanguageData = new ArrayList<>();
        mLanguageData.addAll(languages);
    }

    @Override
    public StringAdapter.LanguagesViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context = viewGroup.getContext();
        int listItemLayoutId = R.layout.search_by_language_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(listItemLayoutId, viewGroup, false);
        StringAdapter.LanguagesViewHolder viewHolder = new StringAdapter.LanguagesViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull StringAdapter.LanguagesViewHolder holder, int position) {
        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_REGION) | mLanguageData.get(position).equals(context.getResources().getStringArray(R.array.languages)[4]) )
        {
            holder.languageCheckBox.setVisibility(View.GONE);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            holder.languageTextView.setLayoutParams(lp);
            holder.languageTextView.setGravity(Gravity.CENTER_HORIZONTAL);

        } else
        {
            holder.languageCheckBox.setVisibility(View.VISIBLE);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.START;
            holder.languageTextView.setLayoutParams(lp);
            holder.languageTextView.setGravity(Gravity.START);

        }

        if (languagesSelected.contains(mLanguageData.get(position))) holder.languageCheckBox.setChecked(true);
            else holder.languageCheckBox.setChecked(false);

        holder.languageTextView.setText(mLanguageData.get(position));
        holder.itemView.setContentDescription(holder.languageTextView.getText().toString());
        holder.itemView.setFocusable(true);
    }

    @Override
    public int getItemCount() {
        return mLanguageData.size();
    }

    public String getLanguageData(int item) {
        return mLanguageData.get(item);
    }

    public void clear() {
        mLanguageData.clear();
        update();
    }

    public void addAll(ArrayList<String> items) {
        mLanguageData.addAll(items);
        update();
    }

    public void removeItem(int position) {
        mLanguageData.remove(position);
        notifyItemRemoved(position);
    }

    private void update() {
        notifyDataSetChanged();
    }

    public SortedSet<String> getLanguagesSelected() {
        return languagesSelected;
    }

    public void setLanguagesSelected(ArrayList<String> selectedLanguagesList) {
        languagesSelected = new TreeSet<> (selectedLanguagesList);
    }

    public void setSearchMode(String sMode) {
        searchMode = sMode;
    }

    public ArrayList<String> getLanguagesData () {
        return mLanguageData;
    }

    class LanguagesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CheckBox languageCheckBox;
        private TextView languageTextView;

        public LanguagesViewHolder(View itemView) {
            super(itemView);
            languageCheckBox = itemView.findViewById(R.id.checkbox_language_item);
            languageTextView = itemView.findViewById(R.id.textview_language_item);

            itemView.setOnClickListener(this);
            languageCheckBox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int clickedPosition = getAdapterPosition();

                    if (compoundButton.isChecked()) {
                        languagesSelected.add(mLanguageData.get(clickedPosition));
                    }
                        else languagesSelected.remove(mLanguageData.get(clickedPosition));
                }
            });
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(getLanguageData(clickedPosition), clickedPosition);
        }
    }
}
