package com.example.revad.countrieshandbook;

import android.text.TextUtils;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

public class FetchData {

    private FetchData() {
    }

    private static String LOG_TAG = FetchData.class.getName();
    private static final String JSON_CODE_NOT_SPECIFIED = "Not specified";
    private static final String COUNTRY_REQUEST_BASE_URL = "https://restcountries.eu/rest/v2/";
    private static final String COUNTRY_REQUEST_REGION = "region/";
    private static final String COUNTRY_REQUEST_SHORT_DATA = "all?fields=name;capital;flag;languages";
    private static final String COUNTRY_REQUEST_COUNTRY_DATA = "name/";
    private static final String COUNTRY_REQUEST_ALL_LANGUAGES = "all?fields=languages";
    private static final String COUNTRY_REQUEST_ALPHA_LANGUAGE = "alpha/";
    private static final String COUNTRY_REQUEST_LANGUAGE = "lang/";
    private static final String JSON_CODE_COUNTRY_NAME = "name";
    private static final String JSON_CODE_LANGUAGE_NAME = "name";
    private static final String JSON_CODE_LANGUAGES = "languages";
    private static final String JSON_CODE_ALPHA = "alpha3Code";
    private static final String JSON_CODE_LANGUAGE_ISO_NAME = "iso639_1";
    private static final String JSON_CODE_CAPITAL_CITY = "capital";
    private static final String JSON_CODE_FLAG = "flag";

    public static ArrayList<CountryDataShort> fetchCountriesData(String cName) {
        URL url;
        boolean searchByRegion = false;

        if (TextUtils.isEmpty(cName)) { url = createUrl (COUNTRY_REQUEST_BASE_URL + COUNTRY_REQUEST_SHORT_DATA); }
        else
            switch (cName) {
            case "Africa": case "Asia": case "Americas": case "Europe": case "Oceania": {
                url = createUrl (COUNTRY_REQUEST_BASE_URL + COUNTRY_REQUEST_REGION + cName);
                searchByRegion = true;
            }
            break;
            default: url = createUrl (COUNTRY_REQUEST_BASE_URL + COUNTRY_REQUEST_ALPHA_LANGUAGE + cName);
        }

        String jsonResponse = null;
        ArrayList<CountryDataShort> countriesData = new ArrayList<>();
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }

        if (TextUtils.isEmpty(cName) | searchByRegion) {countriesData.addAll(extractCountries(jsonResponse));}
        else countriesData.addAll(extractCountry(jsonResponse));
        return countriesData;
    }

    public static String fetchCountryFullInfo(String cName) {
        URL url = createUrl (COUNTRY_REQUEST_BASE_URL + COUNTRY_REQUEST_COUNTRY_DATA + cName);
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }
        return jsonResponse;
    }

    public static ArrayList<String> fetchLanguagesData() {
        URL url = createUrl (COUNTRY_REQUEST_BASE_URL + COUNTRY_REQUEST_ALL_LANGUAGES);
        String jsonResponse = null;
        int i = 1;
        ArrayList<String> languagesData = new ArrayList<>();
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }
        languagesData.addAll(extractLanguages(jsonResponse));
        return languagesData;
    }

    public static ArrayList<CountryDataShort> fetchCountriesDataWithLanguages(String[] languageData) {
        SortedSet<String> countriesSet = new TreeSet<>();
        for (int i=0; i < languageData.length; i++) {
            String lCode = SearchByLanguageActivity.languageTable.getString(languageData[i]);
            countriesSet.addAll( findCountriesByLanguage (lCode) );
        }

        ArrayList<CountryDataShort> countries = new ArrayList<>();

        for (String name : countriesSet)
        {
            countries.addAll(fetchCountriesData(name));
        }

        return countries;
    }

    private static SortedSet<String> findCountriesByLanguage(String language) {
        URL url = createUrl (COUNTRY_REQUEST_BASE_URL + COUNTRY_REQUEST_LANGUAGE + language);

        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }
        return extractCountriesByLanguages(jsonResponse);
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            Log.e(LOG_TAG, "Empty URL");
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
                jsonResponse = null;
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private static ArrayList<CountryDataShort> extractCountries(String sCountries) {
        ArrayList<CountryDataShort> countries = new ArrayList<>();
        SortedSet<String> languageSet = new TreeSet<>();
        try {
            JSONArray jResults = new JSONArray(sCountries);
            for (int i=0; i<jResults.length(); i++) {
                JSONObject countryProperties = jResults.getJSONObject(i);
                String countryName = countryProperties.optString(JSON_CODE_COUNTRY_NAME, JSON_CODE_NOT_SPECIFIED);
                String countryCaptitalCity = countryProperties.optString(JSON_CODE_CAPITAL_CITY, JSON_CODE_NOT_SPECIFIED);
                String countryFlag = countryProperties.optString(JSON_CODE_FLAG, JSON_CODE_NOT_SPECIFIED);
                countries.add(new CountryDataShort(countryName, countryCaptitalCity, countryFlag, languageSet, ""));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }
        return countries;
    }

    private static ArrayList<CountryDataShort> extractCountry(String sCountries) {
        ArrayList<CountryDataShort> countries = new ArrayList<>();
        SortedSet<String> languageSet = new TreeSet<>();
        try {
                JSONObject countryProperties = new JSONObject(sCountries);
                String countryName = countryProperties.optString(JSON_CODE_COUNTRY_NAME, JSON_CODE_NOT_SPECIFIED);
                String countryCaptitalCity = countryProperties.optString(JSON_CODE_CAPITAL_CITY, JSON_CODE_NOT_SPECIFIED);
                String countryFlag = countryProperties.optString(JSON_CODE_FLAG, JSON_CODE_NOT_SPECIFIED);


                    JSONArray jLanguagesArray = countryProperties.getJSONArray(JSON_CODE_LANGUAGES);
                    for (int j = 0; j < jLanguagesArray.length(); j++) {
                        JSONObject jCountryLanguages = jLanguagesArray.getJSONObject(j);
                        String language = jCountryLanguages.optString(JSON_CODE_LANGUAGE_NAME, JSON_CODE_NOT_SPECIFIED);
                        languageSet.add(language);
                    }


                countries.add(new CountryDataShort(countryName, countryCaptitalCity, countryFlag, languageSet, ""));

        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }
        return countries;
    }

    private static ArrayList<String> extractLanguages(String sLanguages) {
        ArrayList<String> languages = new ArrayList<>();
        SortedSet<String> languageSet = new TreeSet<>();
        final String L_ENGLISH = "English";
        final String L_FRENCH = "French";
        final String L_ARABIC = "Arabic";
        final String L_SPANISH = "Spanish";

        try {
            JSONArray jResults = new JSONArray(sLanguages);
            for (int i=0; i<jResults.length(); i++) {
                JSONObject jLanguagesObject = jResults.getJSONObject(i);
                JSONArray jLanguagesArray = jLanguagesObject.getJSONArray(JSON_CODE_LANGUAGES);
                for (int j=0; j<jLanguagesArray.length(); j++) {
                    JSONObject jCountryLanguages = jLanguagesArray.getJSONObject(j);
                    String language = jCountryLanguages.optString(JSON_CODE_LANGUAGE_NAME, JSON_CODE_NOT_SPECIFIED);
                    String languageISOcode = jCountryLanguages.optString(JSON_CODE_LANGUAGE_ISO_NAME, JSON_CODE_NOT_SPECIFIED);
                    languageSet.add(language);
                    SearchByLanguageActivity.languageTable.putString(language, languageISOcode);
                }
            }
            languageSet.remove(L_ENGLISH);
            languageSet.remove(L_FRENCH);
            languageSet.remove(L_ARABIC);
            languageSet.remove(L_SPANISH);
            languages.addAll(languageSet);
            languages.add(0, L_ENGLISH);
            languages.add(1, L_FRENCH);
            languages.add(2, L_ARABIC);
            languages.add(3, L_SPANISH);

        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }
        return languages;
    }

    private static SortedSet<String> extractCountriesByLanguages(String language) {
        SortedSet<String> countriesSet = new TreeSet<>();
        try {
            JSONArray jResults = new JSONArray(language);
            for (int i=0; i<jResults.length(); i++) {
                JSONObject countryProperties = jResults.getJSONObject(i);
                String countryName = countryProperties.optString(JSON_CODE_ALPHA, JSON_CODE_NOT_SPECIFIED);
                countriesSet.add(countryName);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
            }
        return countriesSet;
    }
}
