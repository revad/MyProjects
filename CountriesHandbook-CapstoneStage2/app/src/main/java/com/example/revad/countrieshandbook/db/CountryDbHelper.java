package com.example.revad.countrieshandbook.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.revad.countrieshandbook.db.CountryContract.CountryEntry;

public class CountryDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "countrieshandbook.db";
    private static final int DATABASE_VERSION = 5;

    public CountryDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String SQL_CREATE_COUNTRIES_TABLE =

                "CREATE TABLE " + CountryEntry.TABLE_NAME + " (" +
                        CountryEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        //CountryEntry.COLUMN_COUNTRY_SHORT_NAME + " VARCHAR(3) NOT NULL, " +
                        CountryEntry.COLUMN_COUNTRY_FULL_NAME + " VARCHAR(50) NOT NULL, " +
                        CountryEntry.COLUMN_COUNTRY_CAPITAL_CITY + " VARCHAR(50) NOT NULL, " +
                        CountryEntry.COLUMN_COUNTRY_FLAG + " VARCHAR(50) NOT NULL, " +
                        CountryEntry.COLUMN_COUNTRY_DATA + " VARCHAR(1000) NOT NULL, " +
                        " UNIQUE (" + CountryEntry.COLUMN_COUNTRY_FULL_NAME + ") ON CONFLICT REPLACE);";

        sqLiteDatabase.execSQL(SQL_CREATE_COUNTRIES_TABLE);

        final String SQL_CREATE_LANGUAGES_TABLE =

                "CREATE TABLE " + CountryContract.LanguageEntry.TABLE_NAME_L + " (" +
                        CountryContract.LanguageEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        CountryContract.LanguageEntry.COLUMN_LANGUAGE_SHORT_NAME + " VARCHAR(3), " +
                        CountryContract.LanguageEntry.COLUMN_LANGUAGE_FULL_NAME + " VARCHAR(50) NOT NULL, " +
                        " UNIQUE (" + CountryContract.LanguageEntry.COLUMN_LANGUAGE_SHORT_NAME + ") ON CONFLICT REPLACE);";

        sqLiteDatabase.execSQL(SQL_CREATE_LANGUAGES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CountryEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CountryContract.LanguageEntry.TABLE_NAME_L);
        onCreate(sqLiteDatabase);
    }
}
