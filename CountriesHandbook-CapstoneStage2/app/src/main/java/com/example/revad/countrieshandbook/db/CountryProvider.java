package com.example.revad.countrieshandbook.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

public class CountryProvider extends ContentProvider {

    public static final int CODE_COUNTRIES = 100;
    public static final int CODE_COUNTRY = 101;

    public static final int CODE_LANGUAGES = 200;
    public static final int CODE_LANGUAGE = 201;

    private static final UriMatcher sUriMatcher;
    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(CountryContract.CONTENT_AUTHORITY, CountryContract.PATH_COUNTRY, CODE_COUNTRIES);
        sUriMatcher.addURI(CountryContract.CONTENT_AUTHORITY, CountryContract.PATH_COUNTRY + "/#", CODE_COUNTRY);

        sUriMatcher.addURI(CountryContract.CONTENT_AUTHORITY, CountryContract.PATH_LANGUAGE, CODE_LANGUAGES);
        sUriMatcher.addURI(CountryContract.CONTENT_AUTHORITY, CountryContract.PATH_LANGUAGE + "/#", CODE_LANGUAGE);
    }

    private CountryDbHelper mHelper;

    @Override
    public boolean onCreate() {
        mHelper = new CountryDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor;
        switch (sUriMatcher.match(uri)) {
            case CODE_COUNTRIES: {
                cursor = mHelper.getReadableDatabase().query(
                        CountryContract.CountryEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CODE_COUNTRY: {
                cursor = mHelper.getReadableDatabase().query(
                        CountryContract.CountryEntry.TABLE_NAME,
                        projection,
                        CountryContract.CountryEntry.COLUMN_COUNTRY_FULL_NAME + " = ? ",
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CODE_LANGUAGES: {
                cursor = mHelper.getReadableDatabase().query(
                        CountryContract.LanguageEntry.TABLE_NAME_L,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CODE_LANGUAGE: {
                cursor = mHelper.getReadableDatabase().query(
                        CountryContract.LanguageEntry.TABLE_NAME_L,
                        projection,
                        CountryContract.LanguageEntry.COLUMN_LANGUAGE_FULL_NAME + " = ? ",
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)){
            case CODE_COUNTRIES:
                return CountryContract.CountryEntry.CONTENT_TYPE;
            case CODE_COUNTRY:
                return CountryContract.CountryEntry.CONTENT_ITEM_TYPE;
            case CODE_LANGUAGES:
                return CountryContract.LanguageEntry.CONTENT_TYPE;
            case CODE_LANGUAGE:
                return CountryContract.LanguageEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final SQLiteDatabase db = mHelper.getWritableDatabase();
        Uri returnUri = null;
        if (sUriMatcher.match(uri) == CODE_COUNTRIES) {
            long _id = db.insert(CountryContract.CountryEntry.TABLE_NAME, null, values);
            if (_id > 0) {
                returnUri =  ContentUris.withAppendedId(CountryContract.CountryEntry.CONTENT_URI, _id);
                getContext().getContentResolver().notifyChange(uri, null);
            }
        }
        else
            if (sUriMatcher.match(uri) == CODE_LANGUAGES) {
                long _id = db.insert(CountryContract.LanguageEntry.TABLE_NAME_L, null, values);
                if (_id > 0) {
                    returnUri =  ContentUris.withAppendedId(CountryContract.LanguageEntry.CONTENT_URI, _id);
                    getContext().getContentResolver().notifyChange(uri, null);
                }
            }
            else
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = mHelper.getWritableDatabase();
        int rows;
        if (sUriMatcher.match(uri) == CODE_COUNTRIES) {
            rows = db.delete(CountryContract.CountryEntry.TABLE_NAME, selection, selectionArgs);
        } else
        if (sUriMatcher.match(uri) == CODE_LANGUAGES) {
            rows = db.delete(CountryContract.LanguageEntry.TABLE_NAME_L, selection, selectionArgs);
        } else
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        if(selection == null || rows != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rows;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = mHelper.getWritableDatabase();
        int rows;

        if (sUriMatcher.match(uri) == CODE_COUNTRY) {
            rows = db.update(CountryContract.CountryEntry.TABLE_NAME, values, selection, selectionArgs);
        } else
        if (sUriMatcher.match(uri) == CODE_LANGUAGE) {
            rows = db.update(CountryContract.LanguageEntry.TABLE_NAME_L, values, selection, selectionArgs);
        } else
            throw new UnsupportedOperationException("Unknown uri: " + uri);

        if(rows != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rows;
    }
}
