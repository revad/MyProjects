package com.example.revad.countrieshandbook;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.revad.countrieshandbook.db.CountryContract;
import java.util.ArrayList;
import java.util.Arrays;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchByLanguageActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<String>>, StringAdapter.ListItemClickListener {

    private StringAdapter mAdapter;
    private ArrayList<String> languages;
    @BindView(R.id.spinner_widget_search_by_language) ProgressBar spinner;
    @BindView(R.id.rv_countries_by_language) RecyclerView mLanguagesAndRegionsList;
    @BindView(R.id.empty_state_view_search_by_language) TextView emptyStateView;
    @BindView(R.id.button_start_search_by_language) Button startSearchButton;
    private static final int LOADER_ID_GET_COUNTRIES_DATA_SHORT = 135247;
    public static Bundle languageTable = new Bundle();
    private String searchMode = MainActivity.SEARCH_MODE_BY_LANGUAGE;
    private ArrayList<String> regions;
    private boolean showAllLanguages = false;
    private static final String SAVED_SEARCH_MODE = "SEARCH_MODE";
    private static final String SAVED_LANGUAGES = "LANGUAGES";
    private static final String SAVED_LANGUAGE_TABLE = "LANGUAGE_TABLE";
    private static final String SAVED_SHOW_ALL_LANGUAGES = "SHOW_ALL_LANGUAGES";
    private static final String SAVED_LANGUAGES_DATA_FROM_ADAPTER = "LANGUAGES_DATA_FROM_ADAPTER";
    private static final String SAVED_SELECTED_LANGUAGES = "SELECTED_LANGUAGES";
    private static final String SAVED_RECYCLERVIEW_POSITION = "RECYCLERVIEW_POSITION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_language);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.search_by_language_tool_bar);
        setSupportActionBar(toolbar);

        GridLayoutManager layoutManager = new GridLayoutManager(this, numberOfColumns());
        mLanguagesAndRegionsList.setLayoutManager(layoutManager);
        mLanguagesAndRegionsList.setHasFixedSize(true);

        if (savedInstanceState != null ) { searchMode = savedInstanceState.getString(SAVED_SEARCH_MODE); }
        else
        if (getIntent().hasExtra(MainActivity.SEARCH_MODE_BY_REGION)) {
            searchMode = MainActivity.SEARCH_MODE_BY_REGION;
        } else
        if (getIntent().hasExtra(MainActivity.SEARCH_MODE_BY_LANGUAGE)) {
            searchMode = MainActivity.SEARCH_MODE_BY_LANGUAGE;
        }

        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_LANGUAGE) ) {
            int additionalLanguages = 0;
            if (savedInstanceState != null) {
                languages = savedInstanceState.getStringArrayList(SAVED_LANGUAGES);
                languageTable = savedInstanceState.getBundle(SAVED_LANGUAGE_TABLE);
                showAllLanguages = savedInstanceState.getBoolean(SAVED_SHOW_ALL_LANGUAGES);

                mAdapter = new StringAdapter(this, savedInstanceState.getStringArrayList(SAVED_LANGUAGES_DATA_FROM_ADAPTER));
                mLanguagesAndRegionsList.setAdapter(mAdapter);

                mAdapter.setLanguagesSelected(savedInstanceState.getStringArrayList(SAVED_SELECTED_LANGUAGES));
                mLanguagesAndRegionsList.setVerticalScrollbarPosition(savedInstanceState.getInt(SAVED_RECYCLERVIEW_POSITION));
                if (!showAllLanguages) additionalLanguages = mAdapter.getItemCount() - 5;
            }
            else {
                languages = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.languages)));
                for (int i = 0; i < 4; i++) {
                    languageTable.putString(getResources().getStringArray(R.array.languages)[i], getResources().getStringArray(R.array.languages_short)[i]);
                }

                String sortOrder = CountryContract.LanguageEntry.COLUMN_LANGUAGE_FULL_NAME + " ASC";
                String[] MAIN_PROJECTION = {CountryContract.LanguageEntry.COLUMN_LANGUAGE_FULL_NAME, CountryContract.LanguageEntry.COLUMN_LANGUAGE_SHORT_NAME};
                Uri mQueryUri = CountryContract.LanguageEntry.CONTENT_URI;
                Cursor cursor = getContentResolver().query(mQueryUri, MAIN_PROJECTION, null, null, sortOrder, null);
                final ArrayList<String> languagesFromDb = new ArrayList<>();
                while (cursor.moveToNext()) {
                    String languageFromDbFull = cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[0]));
                    String languageFromDbShort = cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[1]));
                    languagesFromDb.add(languageFromDbFull);
                    languageTable.putString(languageFromDbFull, languageFromDbShort);
                }
                languages.addAll(0, languagesFromDb);

                mAdapter = new StringAdapter(this, languages);
                mLanguagesAndRegionsList.setAdapter(mAdapter);
                additionalLanguages = languagesFromDb.size();
            }
            final int finalAdditionalLanguages = additionalLanguages;
            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                @Override
                public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                    if (showAllLanguages |
                            (!showAllLanguages && viewHolder.getAdapterPosition() > finalAdditionalLanguages - 1) |
                            mAdapter.getItemCount() == 5) return 0;
                    return super.getSwipeDirs(recyclerView, viewHolder);
                }

                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                    String[] args = {mAdapter.getLanguageData(viewHolder.getAdapterPosition())};
                    getApplicationContext().getContentResolver().delete(
                            CountryContract.LanguageEntry.CONTENT_URI,
                            CountryContract.LanguageEntry.COLUMN_LANGUAGE_FULL_NAME + "=?",
                            args);
                    mAdapter.removeItem(viewHolder.getAdapterPosition());
                }
            };
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mLanguagesAndRegionsList);
        }

        if (searchMode.equals(MainActivity.SEARCH_MODE_BY_REGION)) {
            searchMode = MainActivity.SEARCH_MODE_BY_REGION;
            regions = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.regions)));
            mAdapter = new StringAdapter(this, regions);
            mLanguagesAndRegionsList.setAdapter(mAdapter);
            mAdapter.setSearchMode(MainActivity.SEARCH_MODE_BY_REGION);
            startSearchButton.setVisibility(View.GONE);
        }

        if (!isNetworkAvailable()){
            emptyStateView.setText(R.string.no_network);
            emptyStateView.setVisibility(View.VISIBLE);
            Toast.makeText(this, getString(R.string.search_unavailable_message), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private int numberOfColumns() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthDivider = 600;
        int width = displayMetrics.widthPixels;
        int nColumns = width / widthDivider;
        return nColumns;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putString(SAVED_SEARCH_MODE, searchMode);
        if (languages != null) {
            outState.putStringArrayList(SAVED_LANGUAGES, languages);
            outState.putBundle(SAVED_LANGUAGE_TABLE, languageTable);
            outState.putBoolean(SAVED_SHOW_ALL_LANGUAGES, showAllLanguages);
            outState.putStringArrayList(SAVED_LANGUAGES_DATA_FROM_ADAPTER, mAdapter.getLanguagesData());
            outState.putStringArrayList(SAVED_SELECTED_LANGUAGES, new ArrayList<String>(mAdapter.getLanguagesSelected()));
            outState.putInt(SAVED_RECYCLERVIEW_POSITION, mLanguagesAndRegionsList.getVerticalScrollbarPosition());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SAVED_SEARCH_MODE, searchMode);
        if (languages != null) {
            outState.putStringArrayList(SAVED_LANGUAGES, languages);
            outState.putBundle(SAVED_LANGUAGE_TABLE, languageTable);
            outState.putBoolean(SAVED_SHOW_ALL_LANGUAGES, showAllLanguages);
            outState.putStringArrayList(SAVED_LANGUAGES_DATA_FROM_ADAPTER, mAdapter.getLanguagesData());
            outState.putStringArrayList(SAVED_SELECTED_LANGUAGES, new ArrayList<String>(mAdapter.getLanguagesSelected()));
            outState.putInt(SAVED_RECYCLERVIEW_POSITION, mLanguagesAndRegionsList.getVerticalScrollbarPosition());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public Loader<ArrayList<String>> onCreateLoader(int i, Bundle bundle) {
        spinner.setVisibility(View.VISIBLE);
        return new StringLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<String>> loader, ArrayList<String> strings) {
        spinner.setVisibility(View.GONE);
        if (strings.size() == 0) {
            emptyStateView.setText(R.string.no_countries_found);
            emptyStateView.setVisibility(View.VISIBLE);
            mAdapter.clear();
            return;
        }
        mLanguagesAndRegionsList.scrollToPosition(0);
        emptyStateView.setVisibility(View.INVISIBLE);
        if (mAdapter == null) {
            mAdapter = new StringAdapter(this, strings);
            mLanguagesAndRegionsList.setAdapter(mAdapter);
        }
        else {
            mAdapter.clear();
            mAdapter.addAll(strings);
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<String>> loader) { }

    @Override
    public void onListItemClick(String cardData, int position) {
        if (!isNetworkAvailable()) {
            Toast.makeText(this, getString(R.string.search_unavailable_message), Toast.LENGTH_SHORT).show();
            return;
        }
        if (searchMode == MainActivity.SEARCH_MODE_BY_LANGUAGE && mAdapter.getLanguageData(position).equals(getResources().getStringArray(R.array.languages)[4]) ) {
            spinner.setVisibility(View.VISIBLE);
            Bundle args = new Bundle();
            showAllLanguages = true;
            getLoaderManager().initLoader(LOADER_ID_GET_COUNTRIES_DATA_SHORT, args, this);
        }
        if (searchMode == MainActivity.SEARCH_MODE_BY_REGION) {
            Intent countriesByRegionIntent;
            countriesByRegionIntent = new Intent(SearchByLanguageActivity.this, SearchByNameActivity.class);
            countriesByRegionIntent.putExtra(MainActivity.SEARCH_MODE_BY_REGION, mAdapter.getLanguageData(position));
            startActivity(countriesByRegionIntent);
        }
    }

    public void buttonStartSearchOnClick(View view) {
        if (!isNetworkAvailable()) {
            Toast.makeText(this, getString(R.string.search_unavailable_message), Toast.LENGTH_SHORT).show();
            return;
        }
        if (mAdapter.getLanguagesSelected().isEmpty()) {
            Toast.makeText(this, getString(R.string.select_languages_message), Toast.LENGTH_SHORT).show();
            return;
        }

        String [] lList =  mAdapter.getLanguagesSelected().toArray(new String[1]);

        for (int i=0; i < lList.length; i++) {
            if (!languages.contains(lList[i])) {
                ContentValues cv = new ContentValues();
                cv.put(CountryContract.LanguageEntry.COLUMN_LANGUAGE_FULL_NAME, lList[i]);
                cv.put(CountryContract.LanguageEntry.COLUMN_LANGUAGE_SHORT_NAME, languageTable.getString(lList[i]) );
                getApplicationContext().getContentResolver().insert(CountryContract.LanguageEntry.CONTENT_URI, cv);
            }
        }
        Intent countryListIntent;
        countryListIntent = new Intent(SearchByLanguageActivity.this, SearchByNameActivity.class);
        countryListIntent.putExtra(MainActivity.SEARCH_MODE_BY_LANGUAGE, lList);
        startActivity(countryListIntent);
    }
}
