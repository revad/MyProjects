package com.example.revad.countrieshandbook;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.CardsViewHolder> {

    private ArrayList<String> mCardData;
    final private ListItemClickListener mOnClickListener;
    private Context context;

    public interface ListItemClickListener {
        void onListItemClick(String cardData, int position);
    }

    public CardsAdapter(ListItemClickListener listener, ArrayList<String> cards) {
        mOnClickListener = listener;
        mCardData = new ArrayList<>();
        mCardData.addAll(cards);
    }

    @Override
    public CardsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context = viewGroup.getContext();
        int listItemLayoutId = R.layout.main_activity_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(listItemLayoutId, viewGroup, false);
        CardsViewHolder viewHolder = new CardsViewHolder(view);

        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = params.width ;
        view.setLayoutParams(params);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CardsViewHolder holder, int position) {
        if (position == 4 | position == 5) {
            holder.firstLineTextView.setVisibility(View.GONE);
        }
        else holder.firstLineTextView.setVisibility(View.VISIBLE);
        holder.secondtLineTextView.setText(mCardData.get(position));

        holder.itemView.setContentDescription(holder.firstLineTextView.getText().toString() + holder.secondtLineTextView.getText().toString() );
        holder.itemView.setFocusable(true);
    }

    @Override
    public int getItemCount() {
        return mCardData.size();
    }

    public String getCountryData(int item) {
        return mCardData.get(item);
    }

    public void clear() {
        mCardData.clear();
        update();
    }

    public void addAll(ArrayList<String> items) {
        mCardData.addAll(items);
        update();
    }

    private void update() {
        notifyDataSetChanged();
    }

    class CardsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView firstLineTextView;
        private TextView secondtLineTextView;

        public CardsViewHolder(View itemView) {
            super(itemView);
            firstLineTextView = itemView.findViewById(R.id.lable_first_line);
            secondtLineTextView = itemView.findViewById(R.id.label_second_line);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(getCountryData(clickedPosition), clickedPosition);
        }
    }
}
