package com.example.revad.countrieshandbook;

import android.content.Context;
import android.content.AsyncTaskLoader;
import java.util.ArrayList;

public class StringLoader extends AsyncTaskLoader<ArrayList<String>> {

    public StringLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public ArrayList<String> loadInBackground() {
        return FetchData.fetchLanguagesData();
    }
}
