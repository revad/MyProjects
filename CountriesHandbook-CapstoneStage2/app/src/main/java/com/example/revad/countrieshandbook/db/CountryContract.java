package com.example.revad.countrieshandbook.db;

import android.net.Uri;
import android.provider.BaseColumns;

public class CountryContract {

    public static final String CONTENT_AUTHORITY = "com.example.revad.countrieshandbook";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_COUNTRY = "country";

    public static final class CountryEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_COUNTRY)
                .build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_URI  + "/" + PATH_COUNTRY;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_COUNTRY;
        public static final String TABLE_NAME = "viewed_countries";
        //public static final String COLUMN_COUNTRY_SHORT_NAME = "country_short_name";
        public static final String COLUMN_COUNTRY_FULL_NAME = "country_full_name";
        public static final String COLUMN_COUNTRY_CAPITAL_CITY = "country_capital_city";
        public static final String COLUMN_COUNTRY_FLAG = "country_flag";
        public static final String COLUMN_COUNTRY_DATA = "country_json_string";
    }


    public static final String PATH_LANGUAGE = "language";

    public static final class LanguageEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_LANGUAGE)
                .build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_LANGUAGE;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_LANGUAGE;
        public static final String TABLE_NAME_L = "added_languages";
        public static final String COLUMN_LANGUAGE_SHORT_NAME = "language_short_name";
        public static final String COLUMN_LANGUAGE_FULL_NAME = "language_full_name";
    }
}
