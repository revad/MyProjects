package com.example.revad.countrieshandbook;

import android.content.ContentValues;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import com.example.revad.countrieshandbook.db.CountryContract;

public class ShowCountryInfo extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context appContext;
    private static final String JSON_CODE_NOT_SPECIFIED = "Not specified";
    protected static final String NAME_KEY = "name";
    protected static final String NATIVE_NAME_KEY = "nativeName";
    protected static final String CAPITAL_CITY_KEY = "capital";
    protected static final String FLAG_KEY = "flag";
    protected static final String GEO_KEY = "latlng";
    protected static final String POPULATION_KEY = "population";
    protected static final String CURRENCIES_KEY = "currencies";
    protected static final String LANGUAGES_KEY = "languages";
    protected static final String TIMEZONES_KEY = "timezones";
    protected static final String SYMBOL_KEY = "symbol";
    private String cName;
    private ArrayList<String> languageSet = new ArrayList<>();
    private ArrayList<String> latlng = new ArrayList<>();
    private ArrayList<String> timezones = new ArrayList<>();
    private ArrayList<String> currencies = new ArrayList<>();
    private static String LOG_TAG = ShowCountryInfo.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_country_info);

        appContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.show_info_tool_bar);
        setSupportActionBar(toolbar);

        cName = getIntent().getStringExtra(MainActivity.COUNTRY_NAME_EXTRA);
        if (getIntent().hasExtra(MainActivity.COUNTRY_JSON_DATA_EXTRA)) {
            setupPager( displayData(getIntent().getStringExtra(MainActivity.COUNTRY_JSON_DATA_EXTRA)) );
        }
        else
        {
            if (isNetworkAvailable()) {
                Bundle args = new Bundle();
                args.putString(MainActivity.COUNTRY_NAME_EXTRA, cName);
                new LoadCountryData(args).execute();
            }
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private class LoadCountryData extends AsyncTask<Void, Void, String> {

        Bundle bData;
        String dataToSearch;

        public LoadCountryData (Bundle bundle) {
            bData = bundle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dataToSearch = bData.getString(MainActivity.COUNTRY_NAME_EXTRA);
        }

        protected String doInBackground(Void... params) {

            return FetchData.fetchCountryFullInfo(dataToSearch);
        }

        protected void onPostExecute(String result) {
            Bundle shareData = displayData(result);

            ContentValues cv = new ContentValues();
            cv.put(CountryContract.CountryEntry.COLUMN_COUNTRY_FULL_NAME, shareData.getString(NAME_KEY));
            cv.put(CountryContract.CountryEntry.COLUMN_COUNTRY_CAPITAL_CITY, shareData.getString(CAPITAL_CITY_KEY));
            cv.put(CountryContract.CountryEntry.COLUMN_COUNTRY_FLAG, shareData.getString(FLAG_KEY));
            cv.put(CountryContract.CountryEntry.COLUMN_COUNTRY_DATA, result);
            getApplicationContext().getContentResolver().insert(CountryContract.CountryEntry.CONTENT_URI, cv);

            CountriesHandbookAppWidget.widgetRefreshBroadcast(appContext);
            //CountriesHandbookAppWidget.appWidgetUpdate(appContext);
            //CountriesHandbookAppWidget.updateAllWidgets(appContext, R.layout.countries_handbook_app_widget, CountriesHandbookAppWidget.class);

            setupPager(shareData);
        }
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setupPager(Bundle shareData) {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        InfoFragment infoFragment = new InfoFragment();
        infoFragment.setArguments(shareData);
        adapter.addFragment(infoFragment, getString(R.string.info_tab_title));

        MapFragment mapFragment = new MapFragment();
        Bundle mapBundle = new Bundle();
        mapBundle.putString(NAME_KEY, shareData.getString(NAME_KEY));
        mapBundle.putStringArrayList(GEO_KEY, shareData.getStringArrayList(GEO_KEY));
        mapFragment.setArguments(shareData);
        adapter.addFragment(mapFragment, getString(R.string.map_tab_title));

        viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private Bundle displayData(String jsonString) {
        Bundle shareData = new Bundle();

        try {
            JSONArray jCountryArray = new JSONArray(jsonString);
            JSONObject countryProperties = jCountryArray.getJSONObject(0);
            String countryName = countryProperties.optString(NAME_KEY, JSON_CODE_NOT_SPECIFIED);
            shareData.putString(NAME_KEY, countryName);

            String countryNativeName = countryProperties.optString(NATIVE_NAME_KEY, JSON_CODE_NOT_SPECIFIED);
            shareData.putString(NATIVE_NAME_KEY, countryNativeName);

            String countryCaptitalCity = countryProperties.optString(CAPITAL_CITY_KEY, JSON_CODE_NOT_SPECIFIED);
            shareData.putString(CAPITAL_CITY_KEY, countryCaptitalCity);

            String countryFlag = countryProperties.optString(FLAG_KEY, JSON_CODE_NOT_SPECIFIED);
            shareData.putString(FLAG_KEY, countryFlag);

            String countryPopulation = countryProperties.optString(POPULATION_KEY, JSON_CODE_NOT_SPECIFIED);
            shareData.putString(POPULATION_KEY, countryPopulation);

            JSONArray jLanguagesArray = countryProperties.getJSONArray(LANGUAGES_KEY);
            for (int j = 0; j < jLanguagesArray.length(); j++) {
                JSONObject jCountryLanguages = jLanguagesArray.getJSONObject(j);
                String language = jCountryLanguages.optString(NAME_KEY, JSON_CODE_NOT_SPECIFIED);
                languageSet.add(language);
            }
            shareData.putStringArrayList(LANGUAGES_KEY, languageSet);

            JSONArray jLanLngArray = countryProperties.getJSONArray(GEO_KEY);
            for (int j = 0; j < jLanLngArray.length(); j++) {
                Double jCountryLanLng = jLanLngArray.getDouble(j);
                latlng.add(jCountryLanLng.toString());
            }
            shareData.putStringArrayList(GEO_KEY, latlng);

            JSONArray jTimezonesArray = countryProperties.getJSONArray(TIMEZONES_KEY);
            for (int j = 0; j < jTimezonesArray.length(); j++) {
                String jCountryTemizones = jTimezonesArray.getString(j);
                timezones.add(jCountryTemizones);
            }
            shareData.putStringArrayList(TIMEZONES_KEY, timezones);

            JSONArray jCurrenciesArray = countryProperties.getJSONArray(CURRENCIES_KEY);
            for (int j = 0; j < jCurrenciesArray.length(); j++) {
                JSONObject jCountryCurrencies = jCurrenciesArray.getJSONObject(j);
                String currencyName = jCountryCurrencies.optString(NAME_KEY, JSON_CODE_NOT_SPECIFIED);
                String currencySymbol = jCountryCurrencies.optString(SYMBOL_KEY, JSON_CODE_NOT_SPECIFIED);
                if (currencySymbol.equals("Null")) currencies.add(currencyName);
                    else currencies.add(currencyName + ", " + currencySymbol);
            }
            shareData.putStringArrayList(CURRENCIES_KEY, currencies);
        } catch (JSONException e) {
            Log.e(LOG_TAG, getString(R.string.json_parsing_problem_message), e);
        }
        return shareData;
    }
}
