package com.example.revad.newsfeed;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<NewsData>> {

    public static final String LOG_TAG = MainActivity.class.getName();
    private static final String NEWS_REQUEST_URL =
            "https://content.guardianapis.com/search";
    private NewsAdapter adapter;
    private TextView emptyStateView;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView newListView = (ListView) findViewById(R.id.list);
        emptyStateView = (TextView) findViewById(R.id.empty_state_view);
        spinner = (ProgressBar) findViewById(R.id.spinner_widget);
        newListView.setEmptyView(emptyStateView);

        adapter = new NewsAdapter (this, new ArrayList<NewsData>());

        newListView.setAdapter(adapter);

        newListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                NewsData currentNewsData = adapter.getItem(position);

                Uri newUri = Uri.parse(currentNewsData.getUrl());

                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, newUri);

                startActivity(websiteIntent);
            }
        });

        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (isConnected){
            getLoaderManager().initLoader(1, null, this);
        }
        else
        {
            spinner.setVisibility(View.GONE);
            emptyStateView.setText(R.string.no_network);
        }
    }

    @Override
    public Loader<List<NewsData>> onCreateLoader(int i, Bundle bundle) {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String mSortBy = sharedPrefs.getString(
                getString(R.string.settings_sort_by_key),
                getString(R.string.settings_sort_by_default));

        String mTopic = sharedPrefs.getString(
                getString(R.string.settings_topic_key),
                getString(R.string.settings_topic_default)
        );

        Uri baseUri = Uri.parse(NEWS_REQUEST_URL);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        uriBuilder.appendQueryParameter("q", mTopic);
        uriBuilder.appendQueryParameter("order-by", mSortBy);
        uriBuilder.appendQueryParameter("page-size", "50");
        uriBuilder.appendQueryParameter("api-key", "test");

        Log.i(LOG_TAG, uriBuilder.toString());
        return new NewsLoader(this, uriBuilder.toString());
    }

    @Override
    public void onLoadFinished(Loader<List<NewsData>> loader, List<NewsData> news) {
        spinner.setVisibility(View.GONE);
        adapter.clear();
        emptyStateView.setText(R.string.no_news_found);
        if (news == null) {
            return;
        }
        adapter.addAll(news);
    }

    @Override
    public void onLoaderReset(Loader<List<NewsData>> loader) {
        adapter.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
