package com.example.revad.newsfeed;

import android.content.AsyncTaskLoader;
import android.content.Context;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by revad on 3/16/2018.
 */

public class NewsLoader extends AsyncTaskLoader<List<NewsData>> {

    private String mUrl;

    public NewsLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<NewsData> loadInBackground() {
        if (mUrl == null) {
            return null;
        }
        ArrayList<NewsData> news = FetchUtils.fetchNewsData(mUrl);
        return news;
    }
}
