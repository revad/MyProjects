package com.example.revad.popularmovies;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {

    private ArrayList<Movie> mMovieData;
    final private ListItemClickListener mOnClickListener;
    private Context context;
    private static final String MOVIE_POSTER_URL = "http://image.tmdb.org/t/p/w185/";

    public interface ListItemClickListener {
        void onListItemClick(Movie movieData);
    }

    public MoviesAdapter(ListItemClickListener listener, ArrayList<Movie> movies) {
        mOnClickListener = listener;
        mMovieData = new ArrayList<>();
        mMovieData.addAll(movies);
    }

    @Override
    public MoviesViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context = viewGroup.getContext();
        int listItemLayoutId = R.layout.list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(listItemLayoutId, viewGroup, false);
        MoviesViewHolder viewHolder = new MoviesViewHolder(view);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int displayWidth = metrics.widthPixels;
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = (int) displayWidth /2;
        params.height = (int) (params.width * 1.5);
        view.setLayoutParams(params);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesViewHolder holder, int position) {
        Picasso.with(context)
                .load(MOVIE_POSTER_URL + mMovieData.get(position).getPoster())
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.error_placeholder)
                .into(holder.gridItemView);
    }

    @Override
    public int getItemCount() {
        return mMovieData.size();
    }

    private Movie getMovieData(int item) {
        return mMovieData.get(item);
    }

    public void clear() {
        mMovieData.clear();
        update();
    }

    public void addAll(ArrayList<Movie> items) {
        mMovieData.addAll(items);
        update();
    }

    private void update() {
        notifyDataSetChanged();
    }

    class MoviesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView gridItemView;

        public MoviesViewHolder(View itemView) {
            super(itemView);
            gridItemView = itemView.findViewById(R.id.posterView);
            gridItemView.setScaleType(ImageView.ScaleType.FIT_XY);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(getMovieData(clickedPosition));
        }
    }
}
