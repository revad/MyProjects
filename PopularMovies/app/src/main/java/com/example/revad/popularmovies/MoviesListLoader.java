package com.example.revad.popularmovies;

import android.content.AsyncTaskLoader;
import android.content.Context;
import java.util.ArrayList;

public class MoviesListLoader extends AsyncTaskLoader<ArrayList<Movie>> {

    private String mUrl;

    public MoviesListLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public ArrayList<Movie> loadInBackground() {
        if (mUrl == null) {
            return null;
        }
        ArrayList<Movie> movies;
        movies = FetchData.fetchMoviesData(mUrl);
        return movies;
    }
}
