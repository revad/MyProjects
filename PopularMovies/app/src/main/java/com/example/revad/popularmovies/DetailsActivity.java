package com.example.revad.popularmovies;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.movie_title_value) TextView mTitle;
    @BindView(R.id.movie_poster_view) ImageView mPoster;
    @BindView(R.id.movie_summary_value) TextView mSummary;
    @BindView(R.id.movie_rating_value) TextView mRating;
    @BindView(R.id.movie_release_date_value) TextView mReleaseDate;
    private final String MOVIE_TITLE_EXTRA = "movieTitle";
    private final String MOVIE_POSTER_EXTRA = "moviePoster";
    private final String MOVIE_SUMMARY_EXTRA = "movieSummary";
    private final String MOVIE_RATING_EXTRA = "movieRating";
    private final String MOVIE_RELEASE_DATE_EXTRA = "movieReleaseDate";
    private final String MOVIE_POSTER_URL = "http://image.tmdb.org/t/p/w185/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();

        mTitle.setText(extras.getString(MOVIE_TITLE_EXTRA));
        mSummary.setText(extras.getString(MOVIE_SUMMARY_EXTRA));
        mRating.setText(extras.getString(MOVIE_RATING_EXTRA));
        mReleaseDate.setText(extras.getString(MOVIE_RELEASE_DATE_EXTRA));
        Picasso.with(this)
                .load(MOVIE_POSTER_URL + extras.getString(MOVIE_POSTER_EXTRA))
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.error_placeholder)
                .into(mPoster);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        ViewGroup.LayoutParams params = mPoster.getLayoutParams();
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            params.width = displayWidth - 300;
            params.height = (int) (params.width * 1.5);
            mPoster.setLayoutParams(params);
        } else {
            params.width = (int) displayWidth / 2 - 300;
            params.height = (int) (params.width * 1.5);
            mPoster.setLayoutParams(params);
        }
    }
}
