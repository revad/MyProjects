package com.example.revad.popularmovies;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Movie>>, MoviesAdapter.ListItemClickListener {

    private static final String MOVIE_DB_API_KEY = "Put your themoviedb.org API key here";
    private static final String MOVIES_REQUEST_BASE_URL = "https://api.themoviedb.org/3/";
    private static final String MOVIES_REQUEST_URL_BY_POPULARITY = MOVIES_REQUEST_BASE_URL + "movie/popular?api_key=" + MOVIE_DB_API_KEY;
    private static final String MOVIES_REQUEST_URL_BY_RATING = MOVIES_REQUEST_BASE_URL + "movie/top_rated?api_key=" + MOVIE_DB_API_KEY;
    private static final int LOADER_ID_GET_MOVIES_LIST = 761293;
    private static final String MOVIE_TITLE_EXTRA = "movieTitle";
    private static final String MOVIE_POSTER_EXTRA = "moviePoster";
    private static final String MOVIE_SUMMARY_EXTRA = "movieSummary";
    private static final String MOVIE_RATING_EXTRA = "movieRating";
    private static final String MOVIE_RELEASE_DATE_EXTRA = "movieReleaseDate";
    private static final String MOVIES_URL_EXTRA = "movies_url";
    private MoviesAdapter mAdapter;
    private ProgressBar spinner;
    private RecyclerView mMoviesList;
    private TextView emptyStateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMoviesList = (RecyclerView) findViewById(R.id.rv_movies);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        mMoviesList.setLayoutManager(layoutManager);
        mMoviesList.setHasFixedSize(true);
        emptyStateView = (TextView) findViewById(R.id.empty_state_view);
        spinner = (ProgressBar) findViewById(R.id.spinner_widget);

        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (isConnected){
            Bundle args = new Bundle();
            args.putString(MOVIES_URL_EXTRA, MOVIES_REQUEST_URL_BY_POPULARITY);
            getLoaderManager().initLoader(LOADER_ID_GET_MOVIES_LIST, args, this);
        }
        else
        {
            spinner.setVisibility(View.GONE);
            emptyStateView.setText(R.string.no_network);
            emptyStateView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public Loader<ArrayList<Movie>> onCreateLoader(int id, Bundle args) {
        spinner.setVisibility(View.VISIBLE);
        return new MoviesListLoader(this, args.getString(MOVIES_URL_EXTRA));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Movie>> loader, ArrayList<Movie> movies) {
        spinner.setVisibility(View.GONE);
        if (movies.size() == 0) {
            emptyStateView.setText(R.string.no_movies_found);
            emptyStateView.setVisibility(View.VISIBLE);
            return;
        }
        mMoviesList.scrollToPosition(0);
        if (mAdapter == null) {
            mAdapter = new MoviesAdapter(this, movies);
            mMoviesList.setAdapter(mAdapter);
        } else {
            mAdapter.clear();
            mAdapter.addAll(movies);
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Movie>> loader) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Bundle args = new Bundle();
        if (id == R.id.action_popular) {
            args.putString(MOVIES_URL_EXTRA, MOVIES_REQUEST_URL_BY_POPULARITY);
            getLoaderManager().restartLoader(LOADER_ID_GET_MOVIES_LIST, args, this);
        }
        else {
            args.putString(MOVIES_URL_EXTRA, MOVIES_REQUEST_URL_BY_RATING);
            getLoaderManager().restartLoader(LOADER_ID_GET_MOVIES_LIST, args, this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(Movie movieData) {
        Intent movieDetailIntent;
        movieDetailIntent = new Intent(MainActivity.this, DetailsActivity.class);
        movieDetailIntent.putExtra(MOVIE_TITLE_EXTRA, movieData.getTitle());
        movieDetailIntent.putExtra(MOVIE_POSTER_EXTRA, movieData.getPoster());
        movieDetailIntent.putExtra(MOVIE_SUMMARY_EXTRA, movieData.getSummary());
        movieDetailIntent.putExtra(MOVIE_RATING_EXTRA, movieData.getRating());
        movieDetailIntent.putExtra(MOVIE_RELEASE_DATE_EXTRA, movieData.getReleaseDate());
        startActivity(movieDetailIntent);
        getLoaderManager().destroyLoader(LOADER_ID_GET_MOVIES_LIST);
    }
}
