package com.example.revad.app20190715_vadimrebro_nycschools;

//This is a Model class of MVVM pattern, it encapsulates app's data

import android.content.Context;
import android.util.Log;
import androidx.lifecycle.MutableLiveData;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonLiveSatData extends MutableLiveData<SchoolSat> {
    private final Context context;
    private String sDbNum;
    private static final String SAT_REQUEST_URL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json";

    public JsonLiveSatData(Context context, String dbNum){
        this.context = context;
        sDbNum = dbNum;
        LoadData();
    }

    private void LoadData() {
        String url = SAT_REQUEST_URL + "?dbn=" + sDbNum;
        //Creating request dispatch queue with thread pool of dispatchers
        final RequestQueue requestQueue = Volley.newRequestQueue(context);

        //Creating request for retrieving JSONArray response from website
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
            new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    SchoolSat schoolSat;
                    try {
                        JSONObject obj=response.getJSONObject(0);
                        String readingSAT = obj.optString("sat_critical_reading_avg_score", context.getString(R.string.no_reading_sat));
                        String writingSAT = obj.optString("sat_writing_avg_score", context.getString(R.string.no_writing_sat));
                        String mathSAT = obj.optString("sat_math_avg_score", context.getString(R.string.no_math_sat));
                        schoolSat = new SchoolSat(readingSAT, writingSAT, mathSAT);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //If JSON does not contain any SAT data
                        schoolSat = new SchoolSat(context.getString(R.string.no_reading_sat), context.getString(R.string.no_writing_sat), context.getString(R.string.no_math_sat));
                    }
                    //Setting value of LiveData instance
                    setValue(schoolSat);
                }
            },
            new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("VolleyError", error.getMessage());
                }
            }
        );
        //Adding request to dispatch queue
        requestQueue.add(getRequest);
    }
}