package com.example.revad.app20190715_vadimrebro_nycschools;

//This is a ViewModel class of MVVM pattern

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class SatViewModel extends AndroidViewModel {

    @Nullable
    private JsonLiveSatData schoolSat;
    private String mDbNum;

    public SatViewModel(@NonNull Application application, String dbNum) {
        super(application);
        mDbNum = dbNum;
        if(schoolSat==null) {
            schoolSat = new JsonLiveSatData(application, mDbNum);
        }
    }

    public MutableLiveData<SchoolSat> getSatData() {
        return schoolSat;
    }
}
