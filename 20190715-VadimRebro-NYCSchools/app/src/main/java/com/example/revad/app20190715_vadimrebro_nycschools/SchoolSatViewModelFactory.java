package com.example.revad.app20190715_vadimrebro_nycschools;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class SchoolSatViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private String mParam;

    public SchoolSatViewModelFactory(Application application, String param) {
        mApplication = application;
        mParam = param;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new SatViewModel(mApplication, mParam);
    }
}
