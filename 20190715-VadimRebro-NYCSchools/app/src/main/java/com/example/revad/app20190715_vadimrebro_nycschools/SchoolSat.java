package com.example.revad.app20190715_vadimrebro_nycschools;

public class SchoolSat {

    private String mReadingSat;
    private String mWritingSat;
    private String mMathSat;

    public SchoolSat (String readingSat, String writingSat, String mathSat) {
        mReadingSat = readingSat;
        mWritingSat = writingSat;
        mMathSat = mathSat;
    }

    public String getReadingSat() { return mReadingSat; }
    public String getWritingSat() { return mWritingSat; }
    public String getMathSat() { return mMathSat; }
}
