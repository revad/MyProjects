package com.example.revad.bakingrecipes;

import android.app.LoaderManager;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Recipe>>, RecipeAdapter.ListItemClickListener  {

    private static final String RECIPES_REQUEST_BASE_URL = "https://d17h27t6h515a5.cloudfront.net/topher/2017/May/59121517_baking/baking.json";
    private static final int LOADER_ID_GET_RECIPES_LIST = 895614;
    private static final String RECIPES_URL_EXTRA = "recipes_url";
    @BindView(R.id.spinner_widget) ProgressBar spinner;
    @BindView(R.id.empty_state_view) TextView emptyStateView;
    @BindView(R.id.rv_recipes) RecyclerView mRecipeList;
    private RecipeAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            mRecipeList.setLayoutManager(layoutManager);
        }
        else {
            GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
            mRecipeList.setLayoutManager(layoutManager);
        }

        mRecipeList.setHasFixedSize(true);

        ConnectivityManager cm=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork=cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork!=null&&activeNetwork.isConnectedOrConnecting();

        if(isConnected){
            Bundle args=new Bundle();
            args.putString(RECIPES_URL_EXTRA, RECIPES_REQUEST_BASE_URL);
            getLoaderManager().initLoader(LOADER_ID_GET_RECIPES_LIST, args, this);
        }
        else
        {
            spinner.setVisibility(View.GONE);
            emptyStateView.setText(R.string.no_network);
            emptyStateView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public Loader<ArrayList<Recipe>> onCreateLoader(int id,Bundle args){
        spinner.setVisibility(View.VISIBLE);
        return new RecipesLoader(this,args.getString(RECIPES_URL_EXTRA));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Recipe>>loader, ArrayList<Recipe> recipes){
        spinner.setVisibility(View.GONE);

        if (mAdapter == null) {
            mAdapter = new RecipeAdapter(this, recipes);
            mRecipeList.setAdapter(mAdapter);
            updateWidget(0);
        } else {
            mAdapter.clear();
            mAdapter.addAll(recipes);
        }

        if (getIntent().hasExtra("open_recipe")) {
            int rNum = getIntent().getIntExtra("open_recipe", 0);
            getIntent().removeExtra("open_recipe");
            onListItemClick(recipes.get(rNum), rNum);
        }

        if (getIntent().hasExtra("started_by")) {
            Toast.makeText(this, R.string.select_recipe_message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Recipe>> loader){
    }

    @Override
    public void onListItemClick(Recipe recipeData, int position) {
        updateWidget(position);
        if (getIntent().hasExtra("started_by")) {
            MainActivity.this.finish();
            return;
        }
        Intent recipeDetails = new Intent(MainActivity.this, RecipeActivity.class);
        recipeDetails.putExtra("recipe", recipeData);
        startActivity(recipeDetails);
    }

    private void updateWidget (int number) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
        BakingWidget.updateBakingWidgetWithData(getApplicationContext(), appWidgetManager, mAdapter.getRecipeData(number).getName(),  mAdapter.getRecipeData(number).getIngredients(), number);
    }
}