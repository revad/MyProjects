package com.example.revad.bakingrecipes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder> {

    private ArrayList<Recipe> mRecipeData;
    final private ListItemClickListener mOnClickListener;
    private Context context;

    public interface ListItemClickListener {
        void onListItemClick(Recipe recipeData, int position);
    }

    public RecipeAdapter(ListItemClickListener listener, ArrayList<Recipe> recipes) {
        mOnClickListener = listener;
        mRecipeData = new ArrayList<>();
        mRecipeData.addAll(recipes);
    }

    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context = viewGroup.getContext();
        int listItemLayoutId = R.layout.list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(listItemLayoutId, viewGroup, false);
        return new RecipeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder holder, int position) {
        holder.nameView.setText(mRecipeData.get(position).getName());
        holder.dataView.setText(mRecipeData.get(position).getIngredients().size() + context.getString(R.string.dataview_text) +
                mRecipeData.get(position).getSteps().size() + context.getString(R.string.steps_text));
    }

    @Override
    public int getItemCount() {
        return mRecipeData.size();
    }

    public Recipe getRecipeData(int item) {
        return mRecipeData.get(item);
    }

    public void clear() {
        mRecipeData.clear();
        update();
    }

    public void addAll(ArrayList<Recipe> items) {
        mRecipeData.addAll(items);
        update();
    }

    private void update() {
        notifyDataSetChanged();
    }

    class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView gridItemView;
        private TextView nameView;
        private TextView dataView;

        public RecipeViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            nameView = itemView.findViewById(R.id.nameView);
            dataView = itemView.findViewById(R.id.dataView);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(getRecipeData(clickedPosition), clickedPosition);
        }
    }
}
