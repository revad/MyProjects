package com.example.revad.bakingrecipes;

import android.content.AsyncTaskLoader;
import android.content.Context;
import java.util.ArrayList;

public class RecipesLoader extends AsyncTaskLoader<ArrayList<Recipe>> {

    private String mUrl;

    public RecipesLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public ArrayList<Recipe> loadInBackground() {
        if (mUrl == null) {
            return null;
        }
        return FetchData.fetchRecipes(mUrl);
    }
}
