package com.example.revad.bakingrecipes;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Recipe implements Parcelable{

    private String rId;
    private String rName;
    private ArrayList<Ingredient> rIngredients;
    private ArrayList<Step> rSteps;
    private String rServings;
    private String rImage;

    public Recipe (String id, String name, ArrayList<Ingredient> ingredients, ArrayList<Step> steps, String servings, String image) {
        rId = id;
        rName = name;
        rIngredients = ingredients;
        rSteps = steps;
        rServings = servings;
        rImage = image;
    }

    protected Recipe(Parcel in) {
        rId = in.readString();
        rName = in.readString();
        rIngredients = in.createTypedArrayList(Ingredient.CREATOR);
        rSteps = in.createTypedArrayList(Step.CREATOR);
        rServings = in.readString();
        rImage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rId);
        dest.writeString(rName);
        dest.writeTypedList(rIngredients);
        dest.writeTypedList(rSteps);
        dest.writeString(rServings);
        dest.writeString(rImage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    public String getId () {
        return rId;
    }

    public String getName() { return rName; }

    public ArrayList<Ingredient> getIngredients() { return rIngredients; }

    public ArrayList<Step> getSteps() { return rSteps; }

    public String getServings() { return rServings; }

    public String getImage() { return rImage; }

}
