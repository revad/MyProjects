package com.example.revad.bakingrecipes;

import android.os.Parcel;
import android.os.Parcelable;

public class Step implements Parcelable{

    private String sId;
    private String sShortDescription;
    private String sDescription;
    private String sVideoURL;
    private String sThumbnailURL;

    public Step (String id, String shortDescription, String description, String videoURL, String thumbnailURL) {
        sId = id;
        sShortDescription = shortDescription;
        sDescription = description;
        sVideoURL = videoURL;
        sThumbnailURL = thumbnailURL;
    }

    protected Step(Parcel in) {
        sId = in.readString();
        sShortDescription = in.readString();
        sDescription = in.readString();
        sVideoURL = in.readString();
        sThumbnailURL = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sId);
        dest.writeString(sShortDescription);
        dest.writeString(sDescription);
        dest.writeString(sVideoURL);
        dest.writeString(sThumbnailURL);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Step> CREATOR = new Creator<Step>() {
        @Override
        public Step createFromParcel(Parcel in) {
            return new Step(in);
        }

        @Override
        public Step[] newArray(int size) {
            return new Step[size];
        }
    };

    public String getId () {
        return sId;
    }

    public String getShortDescription () {
        return sShortDescription;
    }

    public String getDescription () {
        return sDescription;
    }

    public String getVideoURL ()  {
        return sVideoURL;
    }

    public String getThumbnailURL ()  {
        return sThumbnailURL;
    }
}
