package com.example.revad.bakingrecipes;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class RecipeActivity extends AppCompatActivity implements StepFragment.Callbacks, RecipeFragment.Callbacks{

    private Recipe mRecipe;
    private FragmentManager fragmentManager;
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        mRecipe = getIntent().getParcelableExtra("recipe");
        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            if (findViewById(R.id.two_pane_layout) != null) {
                mTwoPane = true;
                RecipeFragment recipeFragment = new RecipeFragment();
                recipeFragment.setRecipe(mRecipe);
                fragmentManager.beginTransaction()
                        .add(R.id.left_fragment, recipeFragment, "RECIPE_FRAGMENT")
                        .addToBackStack(null)
                        .commit();
            } else {
                mTwoPane = false;
                RecipeFragment recipeFragment = new RecipeFragment();
                recipeFragment.setRecipe(mRecipe);
                fragmentManager.beginTransaction()
                        .add(R.id.recipe_container, recipeFragment, "RECIPE_FRAGMENT")
                        .addToBackStack(null)
                        .commit();
            }
        } else
        {
            if (findViewById(R.id.two_pane_layout) != null) {
                mTwoPane = true;
                RecipeFragment recipeFragment = (RecipeFragment) fragmentManager.getFragment(savedInstanceState,"RECIPE_FRAGMENT");
                StepFragment stepFragment = (StepFragment) fragmentManager.getFragment(savedInstanceState,"STEP_FRAGMENT");

                if (stepFragment != null) {
                    fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    fragmentManager.beginTransaction().remove(stepFragment)
                            .commit();
                    fragmentManager.executePendingTransactions();
                    fragmentManager.beginTransaction().replace(R.id.right_fragment, stepFragment, "STEP_FRAGMENT")
                            .commit();
                    fragmentManager.executePendingTransactions();
                }
                fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction().remove(recipeFragment)
                        .commit();
                fragmentManager.executePendingTransactions();
                fragmentManager.beginTransaction().replace(R.id.left_fragment, recipeFragment, "RECIPE_FRAGMENT")
                        .addToBackStack(null)
                        .commit();
                fragmentManager.executePendingTransactions();
            } else {
                mTwoPane = false;
                RecipeFragment recipeFragment = (RecipeFragment) fragmentManager.getFragment(savedInstanceState,"RECIPE_FRAGMENT");
                StepFragment stepFragment = (StepFragment) fragmentManager.getFragment(savedInstanceState,"STEP_FRAGMENT");

                if (stepFragment != null) {
                    fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    fragmentManager.beginTransaction().remove(stepFragment)
                            .commit();
                    fragmentManager.executePendingTransactions();
                }

                fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction().remove(recipeFragment).commit();
                fragmentManager.executePendingTransactions();
                fragmentManager.beginTransaction().replace(R.id.recipe_container, recipeFragment, "RECIPE_FRAGMENT")
                        .addToBackStack(null)
                        .commit();
                fragmentManager.executePendingTransactions();

                if (stepFragment != null) {
                    fragmentManager.beginTransaction().replace(R.id.recipe_container, stepFragment, "STEP_FRAGMENT")
                            .addToBackStack(null)
                            .commit();
                    fragmentManager.executePendingTransactions();
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragmentManager.putFragment(outState, "RECIPE_FRAGMENT", fragmentManager.findFragmentByTag("RECIPE_FRAGMENT") );
        if (fragmentManager.findFragmentByTag("STEP_FRAGMENT") != null)
            fragmentManager.putFragment(outState, "STEP_FRAGMENT", fragmentManager.findFragmentByTag("STEP_FRAGMENT") );
        outState.putParcelable("RECIPE", mRecipe);
    }

    @Override
    public void onBackButtonClicked(int stepNumber) {
        StepFragment stepFragment = new StepFragment();
        stepFragment.setStep(mRecipe.getSteps().get(stepNumber - 1), stepNumber - 1);
        if (mTwoPane) {
            fragmentManager.beginTransaction()
                    .replace(R.id.right_fragment, stepFragment, "STEP_FRAGMENT")
                    .commit();
        }
        else {
            fragmentManager.beginTransaction()
                    .replace(R.id.recipe_container, stepFragment, "STEP_FRAGMENT")
                    .commit();
        }
    }

    @Override
    public void onNextButtonClicked(int stepNumber) {
        StepFragment stepFragment = new StepFragment();
        stepFragment.setStep(mRecipe.getSteps().get(stepNumber + 1), stepNumber + 1);
        if (mTwoPane) {
            fragmentManager.beginTransaction()
                    .replace(R.id.right_fragment, stepFragment, "STEP_FRAGMENT")
                    .commit();
        }
        else {
            fragmentManager.beginTransaction()
                    .replace(R.id.recipe_container, stepFragment, "STEP_FRAGMENT")
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        int count = fragmentManager.getBackStackEntryCount();
        if (count == 1) {
            super.onBackPressed();
            finish();
        } else {
            fragmentManager.popBackStack();
            android.support.v4.app.Fragment fragment = fragmentManager.findFragmentByTag("STEP_FRAGMENT");
            if (fragment != null) fragmentManager.beginTransaction().remove(fragment).commit();
        }
    }

    @Override
    public void onStepClick(int stepNumber) {
        StepFragment stepFragment = new StepFragment();
        stepFragment.setStep(mRecipe.getSteps().get(stepNumber), stepNumber);
        if (mTwoPane) {
            fragmentManager.beginTransaction()
                    .replace(R.id.right_fragment, stepFragment, "STEP_FRAGMENT")
                    .commit();
        }
        else
        {
            fragmentManager.beginTransaction()
                    .replace(R.id.recipe_container, stepFragment, "STEP_FRAGMENT")
                    .addToBackStack(null)
                    .commit();
        }
    }
}
