package com.example.revad.bakingrecipes;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeFragment extends Fragment {

    private static Recipe mRecipe;
    private RecipeFragment.Callbacks mStepCallbacks;
    @BindView(R.id.recipe_name_textView) TextView rNameText;
    @BindView(R.id.recipe_imageView) ImageView rImageView;
    @BindView(R.id.ingredients_label_textView) TextView rServingsText;

    public interface Callbacks {
        void onStepClick(int stepNumber);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = null;
        if (context instanceof Activity) {
            activity = (Activity) context;
        }
        mStepCallbacks = (RecipeFragment.Callbacks) activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_recipe, container, false);
        ButterKnife.bind(this, view);

        if (TextUtils.isEmpty(mRecipe.getImage()) | mRecipe.getImage().equals("Not specified")) {
            rImageView.setVisibility(View.GONE);
        }
        rNameText.setText(mRecipe.getName());

        LayoutInflater inflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TableLayout table = view.findViewById(R.id.ingredients_list_layout);

        for (int i = 0; i < mRecipe.getIngredients().size(); i++) {

            TableRow rowView = (TableRow)inflater1.inflate(R.layout.ingr_row, null);

            TextView iName = rowView.findViewById(R.id.ingredient_name_view);
            iName.setText(mRecipe.getIngredients().get(i).getIngredient());

            TextView iAmount = rowView.findViewById(R.id.ingredient_amount_view);
            iAmount.setText(mRecipe.getIngredients().get(i).getQuantity());

            TextView iUnits = rowView.findViewById(R.id.ingredient_unit_view);
            iUnits.setText(mRecipe.getIngredients().get(i).getMeasure().toLowerCase());

            table.addView(rowView);
        }

        rServingsText.setText(getString(R.string.servings_text_start) + mRecipe.getServings() + getString(R.string.servings_text_end));

        LinearLayout stepsListLayout = view.findViewById(R.id.steps_list_layout);

        for (int i = 0; i < mRecipe.getSteps().size(); i++) {
            TextView textView = new TextView(getActivity());
            textView.setText(mRecipe.getSteps().get(i).getShortDescription());
            textView.setTextSize(22);
            textView.setTag("textView" + i);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(toPx(16), toPx(1), toPx(16), 0);
            textView.setLayoutParams(params);
            final int stepNum = i;
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mStepCallbacks.onStepClick(stepNum);
                }
            });

            stepsListLayout.addView(textView);
            stepsListLayout.addView(insertDivider());
        }
        return view;
    }

    private int toPx (int value) {
        float d = getActivity().getResources().getDisplayMetrics().density;
        return (int) (value * d);
    }

    private View insertDivider() {
        View divider = new View(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, toPx(1) );
        params.setMargins(toPx(16), 8, toPx(16), 0);
        divider.setLayoutParams(params);
        divider.setBackgroundColor(Color.LTGRAY);
        return divider;
    }

    public static void setRecipe (Recipe recipe) {
        mRecipe = recipe;
    }

    public static Recipe getRecipe () { return mRecipe; }
}
