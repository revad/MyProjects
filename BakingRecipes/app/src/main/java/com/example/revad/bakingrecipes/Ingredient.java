package com.example.revad.bakingrecipes;

import android.os.Parcel;
import android.os.Parcelable;

public class Ingredient implements Parcelable {

    private String iQuantity;
    private String iMeasure;
    private String iIngredient;

    public Ingredient (String quantity, String measure, String ingredient) {
        iQuantity = quantity;
        iMeasure = measure;
        iIngredient = ingredient;
    }

    protected Ingredient(Parcel in) {
        iQuantity = in.readString();
        iMeasure = in.readString();
        iIngredient = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(iQuantity);
        dest.writeString(iMeasure);
        dest.writeString(iIngredient);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
        @Override
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        @Override
        public Ingredient[] newArray(int size) {
            return new Ingredient[size];
        }
    };

    public String getQuantity () {
        return iQuantity;
    }

    public String getMeasure () {
        return iMeasure;
    }

    public String getIngredient () {
        return iIngredient;
    }
}
