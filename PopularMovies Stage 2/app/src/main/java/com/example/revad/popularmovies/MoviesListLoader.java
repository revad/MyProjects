package com.example.revad.popularmovies;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import java.util.ArrayList;

public class MoviesListLoader extends AsyncTaskLoader<ArrayList<Movie>> {

    private String mUrl;
    private String mId;
    private String mKey;
    private Uri mQueryUri;
    private String sortOrder = MovieContract.MovieEntry.COLUMN_MOVIE_TITLE + " ASC";
    private  static final String[] MAIN_PROJECTION = {
            MovieContract.MovieEntry.COLUMN_MOVIE_TITLE,
            MovieContract.MovieEntry.COLUMN_MOVIE_POSTER,
            MovieContract.MovieEntry.COLUMN_MOVIE_SUMMARY,
            MovieContract.MovieEntry.COLUMN_MOVIE_RATING,
            MovieContract.MovieEntry.COLUMN_MOVIE_RELEASEDATE,
            MovieContract.MovieEntry.COLUMN_MOVIE_ID
    };

    public MoviesListLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    public MoviesListLoader(Context context, String key, String id) {
        super(context);
        mId = id;
        mKey = key;
    }

    public MoviesListLoader(Context context, Uri queryUri) {
        super(context);
        mQueryUri = queryUri;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public ArrayList<Movie> loadInBackground() {
        if (mUrl == null & mId == null & mQueryUri ==  null) {
            return null;
        }
        ArrayList<Movie> movies = new ArrayList<>();
        if (!TextUtils.isEmpty(mId)) {
            movies = FetchData.fetchMovieAdditionalData(mKey, mId);
        }
        else if (!TextUtils.isEmpty(mUrl)) {
            movies = FetchData.fetchMoviesData(mUrl);
        }
        else {
            Cursor cursor = getContext().getContentResolver().query(mQueryUri, MAIN_PROJECTION, null,
                    null, sortOrder, null);

            while(cursor.moveToNext()) {
                movies.add( new Movie( cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[0])),
                        null,
                        cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[2])),
                        cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[3])),
                        cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[4])),
                        cursor.getString(cursor.getColumnIndex(MAIN_PROJECTION[5])),
                        null, null,
                        cursor.getBlob(cursor.getColumnIndex(MAIN_PROJECTION[1]))
                ) );
            }

        }
        return movies;
    }
}
