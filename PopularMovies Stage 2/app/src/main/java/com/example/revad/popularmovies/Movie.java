package com.example.revad.popularmovies;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.HashMap;

public class Movie implements Parcelable {

    private String mTitle;
    private String mPoster;
    private String mSummary;
    private String mRating;
    private String mReleaseDate;
    private String mId;
    private HashMap<String, String> mTrailers;
    private HashMap<String, String> mReviews;
    private byte[] mBPoster;

    public Movie (String title, String poster, String summary, String rating, String releaseDate, String id, HashMap<String, String> trailers, HashMap<String, String> reviews, byte[] bposter) {
        mTitle = title;
        mPoster = poster;
        mSummary = summary;
        mRating = rating;
        mReleaseDate = releaseDate;
        mId = id;
        mTrailers = trailers;
        mReviews = reviews;
        mBPoster = bposter;
    }

    public String getTitle () {
        return mTitle;
    }

    public String getPoster () {
        return mPoster;
    }

    public String getSummary () {
        return mSummary;
    }

    public String getRating () {
        return mRating;
    }

    public String getReleaseDate () {
        return mReleaseDate;
    }

    public String getId () {
        return mId;
    }

    public HashMap<String, String> getTrailers () { return mTrailers; }

    public HashMap<String, String> getReviews () { return mReviews; }

    public byte[] getBPoster () { return mBPoster; }

    public void setTrailers (HashMap<String, String> newTrailers) { mTrailers = newTrailers; }

    public void setReviews (HashMap<String, String> newReviews) { mReviews = newReviews; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mPoster);
        dest.writeString(mSummary);
        dest.writeString(mRating);
        dest.writeString(mReleaseDate);
        dest.writeString(mId);
        dest.writeMap(mTrailers);
        dest.writeMap(mReviews);
        dest.writeByteArray(mBPoster);
    }

    private Movie(Parcel in){
        mTitle = in.readString();
        mPoster = in.readString();
        mSummary = in.readString();
        mRating = in.readString();
        mReleaseDate = in.readString();
        mId = in.readString();
        mTrailers = new HashMap<String, String>();
        in.readMap(mTrailers, String.class.getClassLoader());
        mReviews = new HashMap<String, String>();
        in.readMap(mReviews, String.class.getClassLoader());
        mBPoster = in.createByteArray();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
