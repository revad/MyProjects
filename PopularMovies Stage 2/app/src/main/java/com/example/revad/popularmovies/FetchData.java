package com.example.revad.popularmovies;

import android.text.TextUtils;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

public class FetchData {

    private FetchData() {
    }

    private static String LOG_TAG = FetchData.class.getName();
    private static final String JSON_CODE_TITLE = "title";
    private static final String JSON_CODE_RATING = "vote_average";
    private static final String JSON_CODE_POSTER = "poster_path";
    private static final String JSON_CODE_SUMMARY = "overview";
    private static final String JSON_CODE_RELEASE_DATE = "release_date";
    private static final String JSON_CODE_ID = "id";
    private static final String JSON_CODE_KEY = "key";
    private static final String JSON_CODE_NAME = "name";
    private static final String JSON_CODE_URL = "url";
    private static final String JSON_CODE_AUTHOR = "author";
    private static final String JSON_CODE_NOT_SPECIFIED = "Not specified";
    private static final String MOVIE_REQUEST_TRAILERS_URL = "/videos?api_key=";
    private static final String MOVIE_REQUEST_REVIEWS_URL = "/reviews?api_key=";
    private static final String MOVIES_REQUEST_BASE_URL = "https://api.themoviedb.org/3/movie/";
    private static final String MOVIE_TRAILER_BASE_URL = "https://www.youtube.com/watch?v=";

    public static ArrayList<Movie> fetchMoviesData(String requestUrl) {
        URL url;
        String jsonResponse = null;
        String finalUrl;
        int i = 1;
        ArrayList<Movie> movies = new ArrayList<>();
        do {
            finalUrl = requestUrl + "&page=" + i;
            i++;
            url = createUrl(finalUrl);
            try {
                jsonResponse = makeHttpRequest(url);
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error closing input stream", e);
            }
            if (jsonResponse != null) {
                movies.addAll(extractMovies(jsonResponse));
            }
        } while (jsonResponse != null );
        return movies;
    }

    public static ArrayList<Movie> fetchMovieAdditionalData(String key, String id) {
        URL url = createUrl(MOVIES_REQUEST_BASE_URL + id + MOVIE_REQUEST_TRAILERS_URL + key);
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }
        HashMap<String, String> trailersData = extractTrailersData(jsonResponse);

        url = createUrl(MOVIES_REQUEST_BASE_URL + id + MOVIE_REQUEST_REVIEWS_URL + key);
        jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }
        HashMap<String, String> reviewsData = extractReviewsData(jsonResponse);

        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(new Movie(null, null, null, null, null, id, trailersData, reviewsData, null));
        return movies;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            Log.e(LOG_TAG, "Empty URL");
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
                jsonResponse = null;
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private static ArrayList<Movie> extractMovies(String sMovies) {
        ArrayList<Movie> movies = new ArrayList<>();
        try {
            JSONObject jObject = new JSONObject(sMovies);
            JSONArray jResults = jObject.getJSONArray("results");
            for (int i=0; i<jResults.length(); i++) {
                JSONObject movieProperties = jResults.getJSONObject(i);
                String movieTitle = movieProperties.optString(JSON_CODE_TITLE, JSON_CODE_NOT_SPECIFIED);
                String movieRating = movieProperties.optString(JSON_CODE_RATING, JSON_CODE_NOT_SPECIFIED);
                String moviePosterPath = movieProperties.optString(JSON_CODE_POSTER, JSON_CODE_NOT_SPECIFIED);
                String movieSummary = movieProperties.optString(JSON_CODE_SUMMARY, JSON_CODE_NOT_SPECIFIED);
                String movieReleaseDate = movieProperties.optString(JSON_CODE_RELEASE_DATE, JSON_CODE_NOT_SPECIFIED);
                String movieId = movieProperties.optString(JSON_CODE_ID, JSON_CODE_NOT_SPECIFIED);
                movies.add(new Movie(movieTitle, moviePosterPath, movieSummary, movieRating, movieReleaseDate, movieId, null, null, null));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }
        return movies;
    }

    private static HashMap<String, String> extractTrailersData (String sTrailers) {
        if (TextUtils.isEmpty(sTrailers)) return null;
        HashMap<String, String> trailers = new HashMap<>();
        try {
            JSONObject jObject = new JSONObject(sTrailers);
            JSONArray jResults = jObject.getJSONArray("results");
            for (int i=0; i<jResults.length(); i++) {
                JSONObject trailerItem = jResults.getJSONObject(i);
                String trailerKey = trailerItem.optString(JSON_CODE_KEY, JSON_CODE_NOT_SPECIFIED);
                String trailerName = trailerItem.optString(JSON_CODE_NAME, JSON_CODE_NOT_SPECIFIED);
                if (!trailerKey.equals(JSON_CODE_NOT_SPECIFIED)) trailers.put(MOVIE_TRAILER_BASE_URL + trailerKey, trailerName);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }
        return trailers;
    }

    private static HashMap<String, String> extractReviewsData (String sReviews) {
        if (TextUtils.isEmpty(sReviews)) return null;
        HashMap<String, String> reviews = new HashMap<>();
        try {
            JSONObject jObject = new JSONObject(sReviews);
            JSONArray jResults = jObject.getJSONArray("results");
            for (int i=0; i<jResults.length(); i++) {
                JSONObject reviewItem = jResults.getJSONObject(i);
                String reviewUrl = reviewItem.optString(JSON_CODE_URL, JSON_CODE_NOT_SPECIFIED);
                String reviewAuthor = reviewItem.optString(JSON_CODE_AUTHOR, JSON_CODE_NOT_SPECIFIED);
                if (!reviewUrl.equals(JSON_CODE_NOT_SPECIFIED)) reviews.put(reviewUrl, "Review by " + reviewAuthor);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing JSON results", e);
        }
        return reviews;
    }
}
