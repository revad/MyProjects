package com.example.revad.popularmovies;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.revad.popularmovies.MovieContract.MovieEntry;

public class MovieDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "favoritemovies.db";
    private static final int DATABASE_VERSION = 1;

    public MovieDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String SQL_CREATE_MOVIES_TABLE =

                "CREATE TABLE " + MovieEntry.TABLE_NAME + " (" +
                        MovieEntry._ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        MovieEntry.COLUMN_MOVIE_ID + " VARCHAR(10) NOT NULL, " +
                        MovieEntry.COLUMN_MOVIE_TITLE + " VARCHAR(70) NOT NULL, " +
                        MovieEntry.COLUMN_MOVIE_POSTER + " BLOB NOT NULL, " +
                        MovieEntry.COLUMN_MOVIE_SUMMARY + " VARCHAR(500) NOT NULL, " +
                        MovieEntry.COLUMN_MOVIE_RATING + " REAL NOT NULL, " +
                        MovieEntry.COLUMN_MOVIE_RELEASEDATE + " VARCHAR(10) NOT NULL, " +
                        " UNIQUE (" + MovieEntry.COLUMN_MOVIE_ID + ") ON CONFLICT REPLACE);";

        sqLiteDatabase.execSQL(SQL_CREATE_MOVIES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MovieEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
