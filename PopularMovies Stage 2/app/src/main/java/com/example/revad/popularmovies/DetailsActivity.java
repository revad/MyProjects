package com.example.revad.popularmovies;

import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.movie_title_value) TextView mTitle;
    @BindView(R.id.movie_poster_view) ImageView mPoster;
    @BindView(R.id.movie_summary_value) TextView mSummary;
    @BindView(R.id.movie_rating_value) TextView mRating;
    @BindView(R.id.movie_release_date_value) TextView mReleaseDate;
    @BindView(R.id.expListView) ExpandableListView expListView;
    @BindView(R.id.favorite_movie_mark) ImageView favoriteMovieMark;
    @BindView(R.id.share_trailer) ImageView shareTrailer;
    private final String MOVIE_POSTER_URL = "http://image.tmdb.org/t/p/w185/";
    private static final String EXTRA_KEY = "favorite_movie_id";
    private static final String TAG_FAVORITE = "favorite";
    private static final String TAG_NOT_FAVORITE = "not favorite";
    private ExpandableListAdapter expListAdapter;
    private ArrayList<String> headerList;
    private HashMap<String, ArrayList<String>> childList;

    private static final String[] MOVIE_PROJECTION = {
            MovieContract.MovieEntry.COLUMN_MOVIE_ID,
            MovieContract.MovieEntry.COLUMN_MOVIE_TITLE,
            MovieContract.MovieEntry.COLUMN_MOVIE_POSTER,
            MovieContract.MovieEntry.COLUMN_MOVIE_SUMMARY,
            MovieContract.MovieEntry.COLUMN_MOVIE_RATING,
            MovieContract.MovieEntry.COLUMN_MOVIE_RELEASEDATE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        if (getIntent().hasExtra(EXTRA_KEY)) {
            showFavoriteMovieData(getIntent().getExtras().getString(EXTRA_KEY));
        }
        else showMovieData();
    }

    private void showMovieData() {
        final Movie movieData = getIntent().getParcelableExtra("movie");

        mTitle.setText(movieData.getTitle());
        mSummary.setText(movieData.getSummary());
        mRating.setText(movieData.getRating());
        mReleaseDate.setText(movieData.getReleaseDate());
        if (movieData.getTrailers().size() == 0) { shareTrailer.setVisibility(View.INVISIBLE); }
            else shareTrailer.setVisibility(View.VISIBLE);

        if (movieData.getBPoster() != null) {
            Bitmap theImage = BitmapFactory.decodeByteArray(movieData.getBPoster(), 0, (movieData.getBPoster().length));
            mPoster.setImageBitmap(theImage);
        }
        else
            Picasso.with(this)
                    .load(MOVIE_POSTER_URL + movieData.getPoster())
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.error_placeholder)
                    .into(mPoster);

        imageSetup();

        headerList = new ArrayList<String>();
        childList = new HashMap<String, ArrayList<String>>();
        if (movieData.getTrailers().size() > 0) {
            headerList.add("Trailers");
            Collection<String> valueSet = movieData.getTrailers().values();
            ArrayList<String> listOfValues = new ArrayList<String>(valueSet);
            childList.put(headerList.get(0), listOfValues);
        }
        if (movieData.getReviews().size() > 0) {
            headerList.add("Reviews");
            Collection<String> valueSet = movieData.getReviews().values();
            ArrayList<String> listOfValues = new ArrayList<String>(valueSet);
            childList.put(headerList.get(1), listOfValues);
        }
        expListAdapter = new AdditionalDataAdapter(this, headerList, childList);
        expListView.setAdapter(expListAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                switch (groupPosition) {
                    case 0:
                        String trailerUrl = movieData.getTrailers().keySet().toArray()[childPosition].toString();
                        openUrl(trailerUrl);
                        break;

                    case 1:
                        String reviewUrl = movieData.getReviews().keySet().toArray()[childPosition].toString();
                        openUrl(reviewUrl);
                        break;
                }
                return false;
            }
        });

        shareTrailer.setOnClickListener( new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, movieData.getTrailers().keySet().toArray()[0].toString());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
            }
        });

        favoriteMarkSetup (movieData, null);

        if ( readDataFromDb( movieData.getId() ).getCount() > 0 ){
            DrawableCompat.setTint(favoriteMovieMark.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            favoriteMovieMark.setTag(TAG_FAVORITE);
        } else {
            DrawableCompat.setTint(favoriteMovieMark.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
            favoriteMovieMark.setTag(TAG_NOT_FAVORITE);
        }
    }

    private void showFavoriteMovieData (String movieId) {
        shareTrailer.setVisibility(View.INVISIBLE);
        Cursor movieCursor = readDataFromDb(movieId);
        movieCursor.moveToFirst();
        mTitle.setText(movieCursor.getString(1));
        mSummary.setText(movieCursor.getString(3));
        mRating.setText(movieCursor.getString(4));
        mReleaseDate.setText(movieCursor.getString(5));

        ByteArrayInputStream imageStream = new ByteArrayInputStream(
                movieCursor.getBlob(2)
        );
        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
        mPoster.setImageBitmap(theImage);

        imageSetup();

        DrawableCompat.setTint(favoriteMovieMark.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
        favoriteMovieMark.setTag(TAG_FAVORITE);

        favoriteMarkSetup (null, movieId);
    }

    private void imageSetup () {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        ViewGroup.LayoutParams params = mPoster.getLayoutParams();
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            params.width = displayWidth - 300;
            params.height = (int) (params.width * 1.5);
            mPoster.setLayoutParams(params);
        } else {
            params.width = (int) displayWidth / 2 - 300;
            params.height = (int) (params.width * 1.5);
            mPoster.setLayoutParams(params);
        }
    }

    private void favoriteMarkSetup (final Movie movieData, final String movieId) {
        favoriteMovieMark.setOnClickListener( new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (favoriteMovieMark.getTag().equals(TAG_NOT_FAVORITE)) {
                    DrawableCompat.setTint(favoriteMovieMark.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                    favoriteMovieMark.setTag(TAG_FAVORITE);
                    String _id;
                    String _title;
                    String _summary;
                    String _rating;
                    String _releasedate;
                    if (movieData != null) {
                        _id = movieData.getId();
                        _title = movieData.getTitle();
                        _summary = movieData.getSummary();
                        _rating = movieData.getRating();
                        _releasedate = movieData.getReleaseDate();
                    }
                    else
                    {
                        _id = movieId;
                        _title = mTitle.getText().toString();
                        _summary = mSummary.getText().toString();
                        _rating = mRating.getText().toString();
                        _releasedate = mReleaseDate.getText().toString();
                    }
                    ContentValues cv = new ContentValues();
                    cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_ID, _id);
                    cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_TITLE, _title);
                    cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_POSTER, convertBitmapToByteArray());
                    cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_SUMMARY, _summary);
                    cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_RATING, _rating);
                    cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_RELEASEDATE, _releasedate);
                    getApplicationContext().getContentResolver().insert(MovieContract.MovieEntry.CONTENT_URI, cv);
                }
                else {
                    DrawableCompat.setTint(favoriteMovieMark.getDrawable(), ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
                    favoriteMovieMark.setTag(TAG_NOT_FAVORITE);
                    String _id;
                    if (movieData != null) _id = movieData.getId();
                        else _id = movieId;
                    getApplicationContext().getContentResolver().delete(MovieContract.MovieEntry.CONTENT_URI, MovieContract.MovieEntry.COLUMN_MOVIE_ID + "=" + _id, null);
                }
            }
        });
    }

    private void openUrl(String url) {
        Uri website = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, website);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private Cursor readDataFromDb (String id) {
        return getApplicationContext().getContentResolver().query(
                MovieContract.MovieEntry.CONTENT_URI,
                MOVIE_PROJECTION,
                MovieContract.MovieEntry.COLUMN_MOVIE_ID + " = " + id,
                null,
                null
        );
    }

    private byte[] convertBitmapToByteArray() {
        Bitmap bitmap = ((BitmapDrawable)mPoster.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }
}
