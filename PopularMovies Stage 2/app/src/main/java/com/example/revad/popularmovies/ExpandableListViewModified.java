package com.example.revad.popularmovies;

//The code for this class was borrowed at: https://stackoverflow.com/questions/15651288/getchildview-not-being-called

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

public class ExpandableListViewModified extends ExpandableListView {
    boolean expanded = true;

    public ExpandableListViewModified(Context context) { super(context); }

    public ExpandableListViewModified(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandableListViewModified(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public boolean isExpanded() { return expanded; }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (isExpanded()) {
            int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);

            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = getMeasuredHeight();
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}