package com.example.revad.popularmovies;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;

public class AdditionalDataAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private ArrayList<String> mHeaderList;
    private HashMap<String, ArrayList<String>> mChildList;
    private int mItemHeight = 0;

    public AdditionalDataAdapter (Context context, ArrayList<String> headerList, HashMap<String, ArrayList<String>> childList) {
        mContext = context;
        mHeaderList = headerList;
        mChildList = childList;
    }

    @Override
    public int getGroupCount() {
        return mHeaderList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mChildList.get(mHeaderList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mHeaderList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mChildList.get(mHeaderList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return mItemHeight;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater lInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = lInflater.inflate(R.layout.expandablelist_group, null);
        }

        TextView listHeaderText = convertView.findViewById(R.id.listHeaderView);
        listHeaderText.setText(headerTitle);
        mItemHeight = listHeaderText.getHeight();
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater lInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = lInflater.inflate(R.layout.expandablelist_item, null);
        }

        TextView listChildText = convertView.findViewById(R.id.listChildView);
        listChildText.setText(childText);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
