package com.example.revad.booklisting;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

public class SearchOptionsFragment extends DialogFragment {

    public interface OnOptionsDoneListener {
        void onFragmentInteraction(boolean search_author, boolean search_title, boolean search_fulltext, boolean search_free);
    }
    private OnOptionsDoneListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (OnOptionsDoneListener) activity;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.search_options, null);

        final CheckBox chAuthor = (CheckBox) v.findViewById(R.id.search_author_check);
        final CheckBox chTitle = (CheckBox) v.findViewById(R.id.search_title_check);
        final CheckBox chFulltext = (CheckBox) v.findViewById(R.id.search_fulltext_check);
        final CheckBox chFree = (CheckBox) v.findViewById(R.id.search_free_check);
        Button button = (Button) v.findViewById(R.id.setOptionsButton);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mListener.onFragmentInteraction(chAuthor.isChecked(), chTitle.isChecked(), chFulltext.isChecked(), chFree.isChecked());
                getDialog().dismiss();
            }
        });
        return v;
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}
