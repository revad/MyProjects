package com.example.revad.booklisting;

import android.content.AsyncTaskLoader;
import android.content.Context;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by revad on 3/14/2018.
 */

public class BooksListLoader extends AsyncTaskLoader<List<Book>> {

    private String mUrl;
    private String mSchoolName;

    public BooksListLoader(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<Book> loadInBackground() {
        if (mUrl == null) {
            return null;
        }
        ArrayList<Book> books;
            books = FetchData.fetchBooksData(mUrl);
        return books;
    }
}
