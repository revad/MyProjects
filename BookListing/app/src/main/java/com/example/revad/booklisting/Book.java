package com.example.revad.booklisting;

/**
 * Created by revad on 3/14/2018.
 */

public class Book {

    private String[] mAuthor;
    private String mTitle;
    private String mDescription;

    public Book (String[] author, String title, String description) {
        mAuthor = author;
        mTitle = title;
        mDescription = description;
    }

    public String[] getAuthor () {return mAuthor;}

    public String getTitle () {return mTitle;}

    public String getDescription() {return mDescription;}
}
