package com.example.revad.booklisting;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class BooksListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Book>> {

    String searchLink;
    private static final int LOADER_ID_GET_BOOKS_LIST = 1025473;
    private BooksAdapter adapter;
    private TextView emptyStateView;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_list);
        searchLink = getIntent().getExtras().getString("search_link");
        ListView schoolsListView = (ListView) findViewById(R.id.list);
        emptyStateView = (TextView) findViewById(R.id.empty_state_view);
        spinner = (ProgressBar) findViewById(R.id.spinner_widget);
        schoolsListView.setEmptyView(emptyStateView);
        adapter = new BooksAdapter (this, new ArrayList<Book>());
        schoolsListView.setAdapter(adapter);

        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (isConnected){
            getLoaderManager().initLoader(LOADER_ID_GET_BOOKS_LIST, null, this);
        }
        else
        {
            spinner.setVisibility(View.GONE);
            emptyStateView.setText(R.string.no_network);
        }
    }

    @Override
    public Loader<List<Book>> onCreateLoader(int i, Bundle bundle) {
        spinner.setVisibility(View.VISIBLE);
        return new BooksListLoader(this, searchLink);
    }

    @Override
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> books) {
        spinner.setVisibility(View.GONE);
        adapter.clear();
        emptyStateView.setText(R.string.no_books_found);
        if (books == null) {
            return;
        }
        adapter.addAll(books);
    }

    @Override
    public void onLoaderReset(Loader<List<Book>> loader) {
        adapter.clear();
    }
}
