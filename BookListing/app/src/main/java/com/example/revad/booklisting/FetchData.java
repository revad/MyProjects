package com.example.revad.booklisting;

/**
 * Created by revad on 3/14/2018.
 */

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import static com.example.revad.booklisting.MainActivity.LOG_TAG;

public class FetchData {

    private FetchData() {
    }

    public static ArrayList<Book> fetchBooksData(String requestUrl) {
        URL url = createUrl(requestUrl);

        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }

        ArrayList<Book> books = extractBooks(jsonResponse);

        return books;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            Log.e(LOG_TAG, "Empty URL");
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    public static ArrayList<Book> extractBooks(String booksJSON) {
        ArrayList<Book> books = new ArrayList<>();
        try {
            JSONObject jBooks = new JSONObject((booksJSON));

            JSONArray itemsArray = jBooks.getJSONArray("items");
            for (int i=0; i<itemsArray.length(); i++) {
                JSONObject singleBook = itemsArray.getJSONObject(i);

                JSONObject volumeInfo = singleBook.getJSONObject("volumeInfo");

                JSONArray mAuthorsJSONArray = volumeInfo.optJSONArray("authors");
                String[] mAuthors;
                if (mAuthorsJSONArray == null) {
                    mAuthors = null;
                    mAuthors = new String[]{"No data about authors."};
                }
                else {
                    mAuthors = null;
                    mAuthors = new String[mAuthorsJSONArray.length()];
                    if (mAuthorsJSONArray.length() > 0)
                        for (int j = 0; j < mAuthorsJSONArray.length(); j++) {
                            mAuthors[j] = mAuthorsJSONArray.get(j).toString();
                        }
                }

                String mTitle = volumeInfo.getString("title");
                String mDescription = volumeInfo.optString("description", "No description for this book.");
                books.add(new Book(mAuthors, mTitle, mDescription));
            }
        } catch (JSONException e) {
            Log.e("FetchData", "Problem parsing JSON results", e);
        }
        return books;
    }
}
