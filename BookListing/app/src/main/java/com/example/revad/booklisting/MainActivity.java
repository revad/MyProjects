package com.example.revad.booklisting;

import android.app.DialogFragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements SearchOptionsFragment.OnOptionsDoneListener{

    public static final String LOG_TAG = MainActivity.class.getName();
    boolean mSearchAuthor;
    boolean mSearchTitle;
    boolean mSearchFulltext;
    boolean mSearchFree;
    EditText searchTextEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchTextEdit = (EditText) findViewById(R.id.searchText);
    }

    @Override
    public void onFragmentInteraction(boolean search_author, boolean search_title, boolean search_fulltext, boolean search_free) {
        mSearchAuthor = search_author;
        mSearchTitle = search_title;
        mSearchFulltext = search_fulltext;
        mSearchFree = search_free;
    }

    public void searchButtonOnClick(View view) {
        Intent bookListIntent = new Intent(this, BooksListActivity.class);
        String searchString = null;
        searchString = "https://www.googleapis.com/books/v1/volumes?q=";

        if (!mSearchAuthor && !mSearchTitle) {
            searchString = searchString + correctStringForQuery(searchTextEdit.getText().toString()).toLowerCase();
        } else {
            if (mSearchTitle) {
                searchString = searchString + "+intitle:" + correctStringForQuery(searchTextEdit.getText().toString()).toLowerCase();
            }
            if (mSearchAuthor) {
                searchString = searchString + "+inauthor:" + correctStringForQuery(searchTextEdit.getText().toString()).toLowerCase();
            }
        }

        if (mSearchFulltext) searchString = searchString + "&filter=full";
        if (mSearchFree) searchString = searchString + "&filter=free-ebooks";
        Log.i(LOG_TAG, searchString);
        bookListIntent.putExtra("search_link", searchString);
        startActivity(bookListIntent);
    }

    private String correctStringForQuery (String stringInitial) {
        String stringCorrected;
        if (stringInitial.contains(" ")) {
            stringCorrected = stringInitial.replace(" ", "+");
            return stringCorrected;
        }
        return stringInitial;
    }

    public void searchOptionsOnClick(View view) {
        DialogFragment searchOptionsFragment;
        searchOptionsFragment = new SearchOptionsFragment();
        searchOptionsFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        searchOptionsFragment.show(getFragmentManager(), "search_ptions");
    }
}
